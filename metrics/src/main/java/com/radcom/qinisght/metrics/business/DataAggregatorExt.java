package com.radcom.qinisght.metrics.business;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.enterprise.context.RequestScoped;

import org.apache.log4j.Logger;

import java.util.HashMap;


import com.radcom.qinisght.metrics.dtos.errcodedistrib.SampleEntry;
import com.radcom.qinisght.metrics.dtos.errcodedistrib.SampleEntryExt;
import com.radcom.qinisght.metrics.dtos.errcodedistrib.SeriesEntry;
import com.radcom.qinisght.metrics.model.diameter.errcodedistrib.DbEntry;
import com.radcom.qinsight.utils.time.TimeUtils;

@RequestScoped
public class DataAggregatorExt {
	
	private static final Logger theLog = Logger.getLogger(DataAggregatorExt.class);
	
	public List<SampleEntryExt> aggregateData( List<DbEntry> lstDbEntries, AggregationType aggType)
	{

		if ( aggType.equals(AggregationType.AGG_TYPE_PER_5MINS))
			return aggregateDataPer5Mins( lstDbEntries);
		if ( aggType.equals(AggregationType.AGG_TYPE_PER_HOUR))
			return aggregateDataPerHour( lstDbEntries);
		if ( aggType.equals(AggregationType.AGG_TYPE_PER_DAY))
			return aggregateDataPerDay( lstDbEntries);
		if ( aggType.equals(AggregationType.AGG_TYPE_PER_MONTH))
			return aggregateDataPerMonth( lstDbEntries);
		if ( aggType.equals(AggregationType.AGG_TYPE_PER_QUARTER))
			return aggregateDataPerQuarter( lstDbEntries);
		if ( aggType.equals(AggregationType.AGG_TYPE_PER_YEAR))
			return aggregateDataPerYear( lstDbEntries);
		
		
		return aggregateDataPerYear( lstDbEntries);
	}
	
	
	private List<SampleEntryExt> sortMapByDate( Map<String, SampleEntryExt> map)
	{
		List<SampleEntryExt> lstRet	=	new ArrayList<SampleEntryExt>();
		SampleEntryAggregator theSamplesAggregator	=	new SampleEntryAggregator();
		// SORT entries in list by DATE
		SortedSet<String> keys = new TreeSet<>(map.keySet());
		for (String key : keys) { 
			SampleEntryExt smpl = map.get(key);
			// Do aggregation by CAUSE
			theSamplesAggregator.aggregateDataBySum(smpl);
			lstRet.add( smpl);
		}
		return lstRet;
	}
	
	public List<SampleEntryExt> aggregateDataPer5Mins( List<DbEntry> lstDbEntries)
	{
		Map<String, SampleEntryExt> map =	new HashMap<String, SampleEntryExt>();
		for ( DbEntry dbEntry : lstDbEntries)	{
			
			// Key is of type "2018-05-29 00:00"
			String date	=	dbEntry.getTE0().substring(0, 16);
			date	=	date.replaceAll("T", " ");
			SampleEntryExt val	=	map.get(date);
			if ( val == null)	{
				val	=	new SampleEntryExt();
				SeriesEntry ent	=	new SeriesEntry( dbEntry.getTE5(), dbEntry.getHC0E1());
				val.addSeriesEntry(ent);
				// Label on chart is of type 19:15, 19:20, 19:25
				val.setLabel( date.substring(11, 16));

				// Date is of type "05/29/2018 19:15"
				val.setDate( TimeUtils.generateAngloSaxonDate(date) + " " + val.getLabel());
				val.setDrilldownEndDateAsString(null);
				val.setDrilldownStartDateAsString(null);
				val.setDrilldownEndDate(null);
				val.setDrilldownStartDate(null);
				
				map.put(date, val);
			}
			else	{
				Integer newVal	=	dbEntry.getTE5();
				if (newVal == null)		{
					theLog.error( "NULL TE5 "+ dbEntry.getTE0());
					continue;
				}
				SeriesEntry ent	=	new SeriesEntry( dbEntry.getTE5(), dbEntry.getHC0E1());
				val.addSeriesEntry(ent);
			}
			
		}
		
//		SampleEntryAggregator theSamplesAggregator	=	new SampleEntryAggregator();		
//		// SORT entries in list by DATE
//		SortedSet<String> keys = new TreeSet<>(map.keySet());
//		for (String key : keys) { 
//			SampleEntryExt smpl = map.get(key);
//			// Do aggregation by CAUSE
//			theSamplesAggregator.aggregateDataBySum(smpl);
//			lstRet.add( smpl);
//		}		
//		return lstRet;		
		return sortMapByDate( map);
	}
	
	public List<SampleEntryExt> aggregateDataPerHour( List<DbEntry> lstDbEntries)
	{
		Map<String, SampleEntryExt> map =	new HashMap<String, SampleEntryExt>();
		for ( DbEntry dbEntry : lstDbEntries)	{
			// Key is of type 2018-05-29 00, 2018-05-29 01, 2018-05-29 11
			String date	=	dbEntry.getTE0().substring(0, 13);
			String isoDateYYYYMMDD	=	dbEntry.getTE0().substring(0, 10);
			date	=	date.replaceAll("T", " ");
			SampleEntryExt val	=	map.get(date);
			if ( val == null)	{
				val	=	new SampleEntryExt();
				SeriesEntry ent	=	new SeriesEntry( dbEntry.getTE5(), dbEntry.getHC0E1());
				val.addSeriesEntry(ent);
				// Label on chart is of type 19:00, 20:00, 21:00
				val.setLabel( date.substring(11, 13) + ":00");

				// Date is of type 05/29/2018 19:00, 05/29/2018 20:00
				val.setDate( TimeUtils.generateAngloSaxonDate(date) + " " + val.getLabel());				
				val.setDrilldownEndDateAsString( date + ":59:59");
				val.setDrilldownStartDateAsString( date + ":00:00");	
				
				val.setDrilldownEndDate( TimeUtils.getLongFromIsoString( isoDateYYYYMMDD, "GMT+0:00"));
				val.setDrilldownStartDate( TimeUtils.getLongFromIsoString( isoDateYYYYMMDD, "GMT+0:00"));	
				map.put(date, val);
			}
			else	{
				Integer newVal	=	dbEntry.getTE5();
				if (newVal == null)		{
					theLog.error( "NULL TE5 "+ dbEntry.getTE0());
					continue;
				}
				SeriesEntry ent	=	new SeriesEntry( dbEntry.getTE5(), dbEntry.getHC0E1());
				val.addSeriesEntry(ent);
			}
			
		}
		return sortMapByDate( map);		
	}
	
	
	public List<SampleEntryExt> aggregateDataPerDay( List<DbEntry> lstDbEntries)
	{
		Map<String, SampleEntryExt> map =	new HashMap<String, SampleEntryExt>();
		for ( DbEntry dbEntry : lstDbEntries)	{
			String date	=	dbEntry.getTE0().substring(0, 10);
			SampleEntryExt val	=	map.get(date);
			if ( val == null)	{
				val	=	new SampleEntryExt();
				SeriesEntry ent	=	new SeriesEntry( dbEntry.getTE5(), dbEntry.getHC0E1());
				val.addSeriesEntry(ent);
				
				val.setLabel( TimeUtils.convertDateFromYYYYMMDDtoMMMDD( date));
				val.setDate( TimeUtils.generateAngloSaxonDate(date));
				val.setDrilldownEndDateAsString(TimeUtils.generateIsoDateEndOfDay(date));
				val.setDrilldownStartDateAsString(TimeUtils.generateIsoDateStartOfDay(date));
				val.setDrilldownEndDate( TimeUtils.getLongFromIsoString( val.getDrilldownEndDateAsString(), "GMT+0:00"));
				val.setDrilldownStartDate( TimeUtils.getLongFromIsoString( val.getDrilldownStartDateAsString(), "GMT+0:00"));	
				
				map.put(date, val);
			}
			else	{
				Integer newVal	=	dbEntry.getTE5();
				if (newVal == null)		{
					theLog.error( "NULL TE5 "+ dbEntry.getTE0());
					continue;
				}
				SeriesEntry ent	=	new SeriesEntry( dbEntry.getTE5(), dbEntry.getHC0E1());
				val.addSeriesEntry(ent);
			}
			
		}
		return sortMapByDate( map);				
	}
	
	public List<SampleEntryExt> aggregateDataPerMonth( List<DbEntry> lstDbEntries)
	{
		Map<String, SampleEntryExt> map =	new HashMap<String, SampleEntryExt>();
		for ( DbEntry dbEntry : lstDbEntries)	{
			String szYearAndMonth	=	dbEntry.getTE0().substring(0, 7);
			String szYearAndMonthAndDay	=	dbEntry.getTE0().substring(0, 10);
			SampleEntryExt val	=	map.get(szYearAndMonth);
			if ( val == null)	{
				val	=	new SampleEntryExt();
				SeriesEntry ent	=	new SeriesEntry( dbEntry.getTE5(), dbEntry.getHC0E1());
				val.addSeriesEntry(ent);
				
				// Label must be of type "May 2018"
				val.setLabel( TimeUtils.convertDateFromYYYYMMDDtoMMMYYYY( dbEntry.getTE0().substring(0, 11))); 
				
				// Date must be of type "05/01/2018", 06/01/2018, 07/01/2018, etc. Always first day of month
				val.setDate( TimeUtils.generateAngloSaxonDate(dbEntry.getTE0().substring(0, 7) + "-01"));
				
				// Drill Down End date must be of type "2018-05-31 23:59:59"
				val.setDrilldownEndDateAsString(TimeUtils.generateIsoDateEndOfMonth(szYearAndMonthAndDay));
				// Drill Down Start date must be of type "2018-05-01 00:00:00"
				val.setDrilldownStartDateAsString(TimeUtils.generateIsoDateStartOfMonth(szYearAndMonthAndDay));
				
				val.setDrilldownEndDate( TimeUtils.getLongFromIsoString( val.getDrilldownEndDateAsString(), "GMT+0:00"));
				val.setDrilldownStartDate( TimeUtils.getLongFromIsoString( val.getDrilldownStartDateAsString(), "GMT+0:00"));	

				
				map.put(szYearAndMonth, val);
			}
			else	{
				Integer newVal	=	dbEntry.getTE5();
				if (newVal == null)		{
					theLog.error( "NULL TE5 "+ dbEntry.getTE0());
					continue;
				}
				SeriesEntry ent	=	new SeriesEntry( dbEntry.getTE5(), dbEntry.getHC0E1());
				val.addSeriesEntry(ent);
			}
			
		}
		return sortMapByDate( map);					
	}
	
	public List<SampleEntryExt> aggregateDataPerQuarter( List<DbEntry> lstDbEntries)
	{

		Map<String, SampleEntryExt> map =	new HashMap<String, SampleEntryExt>();
		for ( DbEntry dbEntry : lstDbEntries)	{
			String szYearAndMonth	=	dbEntry.getTE0().substring(0, 7);
			String szYearAndMonthAndDay	=	dbEntry.getTE0().substring(0, 10);
			String quarter	=	TimeUtils.getQuarterFromDate(szYearAndMonthAndDay);
			SampleEntryExt val	=	map.get(quarter);
			if ( val == null)	{				
				val	=	new SampleEntryExt();
				SeriesEntry ent	=	new SeriesEntry( dbEntry.getTE5(), dbEntry.getHC0E1());
				val.addSeriesEntry(ent);
				
				// Label must be of type "Quarter 1, 2018"
				val.setLabel( quarter); 

				
				// Date must be of type "01/01/2018", "04/01/2018"
				val.setDate( TimeUtils.generateAngloSaxonDateStartOfQuarterFromDateYYYYMMDD(szYearAndMonthAndDay));
				
				// Drill Down End date must be of type "2018-05-31 23:59:59"
				val.setDrilldownEndDateAsString(TimeUtils.generateIsoDateEndOfQuarter(szYearAndMonthAndDay));
				// Drill Down Start date must be of type "2018-05-01 00:00:00"
				val.setDrilldownStartDateAsString(TimeUtils.generateIsoDateStartOfQuarter(szYearAndMonthAndDay));
				
				val.setDrilldownEndDate( TimeUtils.getLongFromIsoString( val.getDrilldownEndDateAsString(), "GMT+0:00"));
				val.setDrilldownStartDate( TimeUtils.getLongFromIsoString( val.getDrilldownStartDateAsString(), "GMT+0:00"));	

				theLog.info( "aggregateDataPerQuarter NEW PUT quarter=" + quarter + " val=" + val);
				
				map.put(quarter, val);
			}
			else	{
				Integer newVal	=	dbEntry.getTE5();
				if (newVal == null)		{
					theLog.error( "NULL TE5 "+ dbEntry.getTE0());
					continue;
				}
				SeriesEntry ent	=	new SeriesEntry( dbEntry.getTE5(), dbEntry.getHC0E1());
				val.addSeriesEntry(ent);
			}
			
		}
		return sortMapByDate( map);	
	}
	
	
	public List<SampleEntryExt> aggregateDataPerYear( List<DbEntry> lstDbEntries)
	{
		Map<String, SampleEntryExt> map =	new HashMap<String, SampleEntryExt>();
		for ( DbEntry dbEntry : lstDbEntries)	{
			String szYear	=	dbEntry.getTE0().substring(0, 4);
			SampleEntryExt val	=	map.get(szYear);
			if ( val == null)	{
				val	=	new SampleEntryExt();
				SeriesEntry ent	=	new SeriesEntry( dbEntry.getTE5(), dbEntry.getHC0E1());
				val.addSeriesEntry(ent);
				
				val.setLabel( szYear);

				val.setDate( "01/01/" + szYear);
				val.setDrilldownEndDateAsString(TimeUtils.generateIsoDateEndOfYear(szYear));
				val.setDrilldownStartDateAsString(TimeUtils.generateIsoDateStartOfYear(szYear));
				
				
				val.setDrilldownEndDate( TimeUtils.getLongFromIsoString( val.getDrilldownEndDateAsString(), "GMT+0:00"));
				val.setDrilldownStartDate( TimeUtils.getLongFromIsoString( val.getDrilldownStartDateAsString(), "GMT+0:00"));	
				
				map.put(szYear, val);
			}
			else	{
				Integer newVal	=	dbEntry.getTE5();
				if (newVal == null)		{
					theLog.error( "NULL TE5 "+ dbEntry.getTE0());
					continue;
				}
				SeriesEntry ent	=	new SeriesEntry( dbEntry.getTE5(), dbEntry.getHC0E1());
				val.addSeriesEntry(ent);
			}
			
		}
		return sortMapByDate( map);	
	}	
	

}
