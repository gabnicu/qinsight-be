package com.radcom.qinisght.metrics.rest.diameter;

import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.radcom.qinisght.metrics.business.MetricsProvider;
import com.radcom.qinisght.metrics.dtos.errcodedistrib.SampleEntry;
import com.radcom.qinisght.metrics.model.diameter.errcodedistrib.MetricsEntry;
import com.radcom.qinisght.metrics.model.diameter.errcodedistrib.accessors.EjbGetMetrics;
import com.radcom.qinisght.metrics.model.diameter.errcodedistrib.dao.DaoErrorCodeDistribution;

import com.radcom.qinsight.utils.file.FileUtil;
import com.radcom.qinsight.utils.rest.dtos.BasicDTO;
import com.radcom.qinsight.utils.time.TimeUtils;

import javax.ws.rs.QueryParam;



@Transactional
@Path("metricsOLD")
public class RestDiameter {
	
	private static final Logger theLog = Logger.getLogger(RestDiameter.class);
	@EJB
	EjbGetMetrics ejbDiameterErrCodeDistrib;
	
	@Inject 
	MetricsProvider metricsProvider;
	

    @GET
    @Path("fixed")
    @Produces({"application/json"})
    public Response getAllFixedJdbc() {


		List<MetricsEntry> lst	=	ejbDiameterErrCodeDistrib.getAll(); 
		BasicDTO dto	=	new BasicDTO();
		dto.setData( lst);	
		return Response.status(Response.Status.OK).entity(dto).build();    	
    }
    
    @GET
    @Path("diameter/errorCodeDistributionExt")
    @Produces({"application/json"})
    public Response getMetricsDiameterErrorCodeDistributionExt( 
    		@QueryParam("startDate") final String startDate, 
    		@QueryParam("endDate") final String endDate, 
    		@QueryParam("networkElements") final String networkElements) 
    {

//    	String startDate	=	"2018-05-14 00:00:00";
//    	String endDate		=	"2018-05-15 23:00:00";
    	String nes	=	null;
    	if ( networkElements == null)
    		nes	=	"'AAA_1', 'AAA_2', 'AAA_3', 'AS_1', 'AS_2', 'AS_3', 'AS_5', 'AS_6', 'AS_7', 'AS_8', 'AS_9', 'CDF', 'CSCF_1', 'CSCF_2', 'HSS_2', 'HSS_3', 'Other', 'PCRF_1', 'PCRF_2', 'PGW_1', 'PGW_2', 'PGW_20', 'PGW_21', 'PRC10', 'PRC11', 'PRC2', 'PRC4', 'PRC5', 'PRC6', 'PRC7', 'PRC8', 'PRC9', 'test1', 'test2', 'test3', 'Unknown_3', 'Unknown_4', 'yoni1', 'yoni1_dns', 'yoni10', 'yoni11', 'yoni12', 'yoni13', 'yoni14', 'yoni15', 'yoni16', 'yoni17', 'yoni18', 'yoni2', 'yoni2_dns', 'yoni3', 'yoni4', 'yoni5', 'yoni6', 'yoni7', 'yoni8', 'yoni9'";    	
    	else if ( networkElements.isEmpty())
    		nes	=	"'AAA_1', 'AAA_2', 'AAA_3', 'AS_1', 'AS_2', 'AS_3', 'AS_5', 'AS_6', 'AS_7', 'AS_8', 'AS_9', 'CDF', 'CSCF_1', 'CSCF_2', 'HSS_2', 'HSS_3', 'Other', 'PCRF_1', 'PCRF_2', 'PGW_1', 'PGW_2', 'PGW_20', 'PGW_21', 'PRC10', 'PRC11', 'PRC2', 'PRC4', 'PRC5', 'PRC6', 'PRC7', 'PRC8', 'PRC9', 'test1', 'test2', 'test3', 'Unknown_3', 'Unknown_4', 'yoni1', 'yoni1_dns', 'yoni10', 'yoni11', 'yoni12', 'yoni13', 'yoni14', 'yoni15', 'yoni16', 'yoni17', 'yoni18', 'yoni2', 'yoni2_dns', 'yoni3', 'yoni4', 'yoni5', 'yoni6', 'yoni7', 'yoni8', 'yoni9'";    	
    	else
    		nes	=	networkElements;
    	List<SampleEntry> lst	=	metricsProvider.getMetrics( startDate, endDate, -180, nes);
    	BasicDTO dto	=	new BasicDTO();
		dto.setData( lst);
		return Response.status(Response.Status.OK).entity(dto).build();    
    }
    
    @GET
    @Path("diameter/errorCodeDistribution")
    @Produces({"application/json"})
    public Response getMetricsDiameterErrorCodeDistribution( 
    		@QueryParam("startDate") final Long startDate, 
    		@QueryParam("endDate") final Long endDate, 
    		@QueryParam("networkElements") final String networkElements,
    		@QueryParam("delta") final Integer delta) 
    {

    	theLog.info( "------ getMetricsDiameterErrorCodeDistribution startDate=" + startDate + 
    			" endDate=" + endDate + " networkElements=" + networkElements + " delta=" + delta);
//    	String startDate	=	"2018-05-14 00:00:00";
//    	String endDate		=	"2018-05-15 23:00:00";
    	String nes	=	null;
    	if ( networkElements == null)
    		nes	=	"'AAA_1', 'AAA_2', 'AAA_3', 'AS_1', 'AS_2', 'AS_3', 'AS_5', 'AS_6', 'AS_7', 'AS_8', 'AS_9', 'CDF', 'CSCF_1', 'CSCF_2', 'HSS_2', 'HSS_3', 'Other', 'PCRF_1', 'PCRF_2', 'PGW_1', 'PGW_2', 'PGW_20', 'PGW_21', 'PRC10', 'PRC11', 'PRC2', 'PRC4', 'PRC5', 'PRC6', 'PRC7', 'PRC8', 'PRC9', 'test1', 'test2', 'test3', 'Unknown_3', 'Unknown_4', 'yoni1', 'yoni1_dns', 'yoni10', 'yoni11', 'yoni12', 'yoni13', 'yoni14', 'yoni15', 'yoni16', 'yoni17', 'yoni18', 'yoni2', 'yoni2_dns', 'yoni3', 'yoni4', 'yoni5', 'yoni6', 'yoni7', 'yoni8', 'yoni9'";    	
    	else if ( networkElements.isEmpty())
    		nes	=	"'AAA_1', 'AAA_2', 'AAA_3', 'AS_1', 'AS_2', 'AS_3', 'AS_5', 'AS_6', 'AS_7', 'AS_8', 'AS_9', 'CDF', 'CSCF_1', 'CSCF_2', 'HSS_2', 'HSS_3', 'Other', 'PCRF_1', 'PCRF_2', 'PGW_1', 'PGW_2', 'PGW_20', 'PGW_21', 'PRC10', 'PRC11', 'PRC2', 'PRC4', 'PRC5', 'PRC6', 'PRC7', 'PRC8', 'PRC9', 'test1', 'test2', 'test3', 'Unknown_3', 'Unknown_4', 'yoni1', 'yoni1_dns', 'yoni10', 'yoni11', 'yoni12', 'yoni13', 'yoni14', 'yoni15', 'yoni16', 'yoni17', 'yoni18', 'yoni2', 'yoni2_dns', 'yoni3', 'yoni4', 'yoni5', 'yoni6', 'yoni7', 'yoni8', 'yoni9'";    	
    	else
    		nes	=	"'AAA_1', 'AAA_2', 'AAA_3', 'AS_1', 'AS_2', 'AS_3', 'AS_5', 'AS_6', 'AS_7', 'AS_8', 'AS_9', 'CDF', 'CSCF_1', 'CSCF_2', 'HSS_2', 'HSS_3', 'Other', 'PCRF_1', 'PCRF_2', 'PGW_1', 'PGW_2', 'PGW_20', 'PGW_21', 'PRC10', 'PRC11', 'PRC2', 'PRC4', 'PRC5', 'PRC6', 'PRC7', 'PRC8', 'PRC9', 'test1', 'test2', 'test3', 'Unknown_3', 'Unknown_4', 'yoni1', 'yoni1_dns', 'yoni10', 'yoni11', 'yoni12', 'yoni13', 'yoni14', 'yoni15', 'yoni16', 'yoni17', 'yoni18', 'yoni2', 'yoni2_dns', 'yoni3', 'yoni4', 'yoni5', 'yoni6', 'yoni7', 'yoni8', 'yoni9'";    	

    	String stdt	=	TimeUtils.getIsoStringFromLong(startDate, null);
    	String endt	=	TimeUtils.getIsoStringFromLong(endDate, null);
    	
    	List<SampleEntry> lst	=	metricsProvider.getMetrics( stdt, endt, -180, nes);
    	BasicDTO dto	=	new BasicDTO();
		dto.setData( lst);
		return Response.status(Response.Status.OK).entity(dto).build();    
    }
    
   
    private String processNetworkElements( String initialString)
    {
    	String items[]	=	initialString.split(",");
    	StringBuffer sBuff	=	new StringBuffer();
    	for ( String itm : items)	{
    		itm	=	itm.trim();
    		if ( !(itm.startsWith("'")))	{
    			itm	=	"'" + itm;
    		}
    		if ( !(itm.endsWith("'")))	{
    			itm	=	itm + "'";
    		}
    		sBuff.append(itm);
    		sBuff.append(",");
    		
    	}
    	return sBuff.toString();
    }
    
    

}
