package com.radcom.qinisght.metrics.business;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.enterprise.context.RequestScoped;

import org.apache.log4j.Logger;

import java.util.HashMap;


import com.radcom.qinisght.metrics.dtos.errcodedistrib.SampleEntry;
import com.radcom.qinisght.metrics.model.diameter.errcodedistrib.DbEntry;
import com.radcom.qinsight.utils.time.TimeUtils;

@RequestScoped
public class DataAggregator {
	
	private static final Logger theLog = Logger.getLogger(DataAggregator.class);
	
	public List<SampleEntry> aggregateData( List<DbEntry> lstDbEntries, AggregationType aggType)
	{

		if ( aggType.equals(AggregationType.AGG_TYPE_PER_5MINS))
			return aggregateDataPer5Mins( lstDbEntries);
		if ( aggType.equals(AggregationType.AGG_TYPE_PER_HOUR))
			return aggregateDataPerHour( lstDbEntries);
		if ( aggType.equals(AggregationType.AGG_TYPE_PER_DAY))
			return aggregateDataPerDay( lstDbEntries);
		if ( aggType.equals(AggregationType.AGG_TYPE_PER_MONTH))
			return aggregateDataPerMonth( lstDbEntries);
		if ( aggType.equals(AggregationType.AGG_TYPE_PER_QUARTER))
			return aggregateDataPerQuarter( lstDbEntries);
		if ( aggType.equals(AggregationType.AGG_TYPE_PER_YEAR))
			return aggregateDataPerYear( lstDbEntries);
		
		
		return aggregateDataPerYear( lstDbEntries);
	}
	
	public List<SampleEntry> aggregateDataPer5Mins( List<DbEntry> lstDbEntries)
	{
		List<SampleEntry> lstRet	=	new ArrayList<SampleEntry>();
		Map<String, SampleEntry> map =	new HashMap<String, SampleEntry>();
		for ( DbEntry dbEntry : lstDbEntries)	{
			
			// Key is of type "2018-05-29 00:00"
			String date	=	dbEntry.getTE0().substring(0, 16);
			date	=	date.replaceAll("T", " ");
			SampleEntry val	=	map.get(date);
			if ( val == null)	{
				val	=	new SampleEntry();
				val.setValue( dbEntry.getTE3());
				// Label on chart is of type 19:15, 19:20, 19:25
				val.setLabel( date.substring(11, 16));
				val.setCause( dbEntry.getHC0E1());
				// Date is of type "05/29/2018 19:15"
				val.setDate( TimeUtils.generateAngloSaxonDate(date) + " " + val.getLabel());
				val.setDrilldownEndDateAsString(null);
				val.setDrilldownStartDateAsString(null);
				val.setDrilldownEndDate(null);
				val.setDrilldownStartDate(null);
				
				map.put(date, val);
			}
			else	{
				Integer newVal	=	dbEntry.getTE3();
				if (newVal == null)		{
					theLog.error( "NULL TE3 "+ dbEntry.getTE0());
					continue;
				}
				val.setValue( val.getValue() + newVal);
			}
			
		}
		
		SortedSet<String> keys = new TreeSet<>(map.keySet());
		for (String key : keys) { 
			SampleEntry smpl = map.get(key);
			generateAndSetDescriptionPer5Mins( smpl);
			lstRet.add( smpl);
		}
		
		return lstRet;
		
	}
	
	public List<SampleEntry> aggregateDataPerHour( List<DbEntry> lstDbEntries)
	{
		List<SampleEntry> lstRet	=	new ArrayList<SampleEntry>();
		Map<String, SampleEntry> map =	new HashMap<String, SampleEntry>();
		for ( DbEntry dbEntry : lstDbEntries)	{
			// Key is of type 2018-05-29 00, 2018-05-29 01, 2018-05-29 11
			String date	=	dbEntry.getTE0().substring(0, 13);
			date	=	date.replaceAll("T", " ");
			SampleEntry val	=	map.get(date);
			if ( val == null)	{
				val	=	new SampleEntry();
				val.setValue( dbEntry.getTE3());
				// Label on chart is of type 19:00, 20:00, 21:00
				val.setLabel( date.substring(11, 13) + ":00");
				val.setCause( dbEntry.getHC0E1());
				// Date is of type 05/29/2018 19:00, 05/29/2018 20:00
				val.setDate( TimeUtils.generateAngloSaxonDate(date) + " " + val.getLabel());				
				val.setDrilldownEndDateAsString( date + ":59:59");
				val.setDrilldownStartDateAsString( date + ":00:00");	
				
				val.setDrilldownEndDate( TimeUtils.getLongFromIsoString( val.getDrilldownEndDateAsString(), null));
				val.setDrilldownStartDate( TimeUtils.getLongFromIsoString( val.getDrilldownStartDateAsString(), null));	
				map.put(date, val);
			}
			else	{
				Integer newVal	=	dbEntry.getTE3();
				if (newVal == null)		{
					theLog.error( "NULL TE3 "+ dbEntry.getTE0());
					continue;
				}
				val.setValue( val.getValue() + newVal);
			}
			
		}
		
		SortedSet<String> keys = new TreeSet<>(map.keySet());
		for (String key : keys) { 
			SampleEntry smpl = map.get(key);
			generateAndSetDescriptionPerHour( smpl);
			lstRet.add( smpl);
		}
		
		return lstRet;
		
	}
	
	
	public List<SampleEntry> aggregateDataPerDay( List<DbEntry> lstDbEntries)
	{
		List<SampleEntry> lstRet	=	new ArrayList<SampleEntry>();
		Map<String, SampleEntry> map =	new HashMap<String, SampleEntry>();
		for ( DbEntry dbEntry : lstDbEntries)	{
			String date	=	dbEntry.getTE0().substring(0, 10);
			SampleEntry val	=	map.get(date);
			if ( val == null)	{
				val	=	new SampleEntry();
				val.setValue( dbEntry.getTE3());
				val.setLabel( TimeUtils.convertDateFromYYYYMMDDtoMMMDD( date));
				val.setCause( dbEntry.getHC0E1());
				val.setDate( TimeUtils.generateAngloSaxonDate(date));
				val.setDrilldownEndDateAsString(TimeUtils.generateIsoDateEndOfDay(date));
				val.setDrilldownStartDateAsString(TimeUtils.generateIsoDateStartOfDay(date));
				val.setDrilldownEndDate( TimeUtils.getLongFromIsoString( val.getDrilldownEndDateAsString(), null));
				val.setDrilldownStartDate( TimeUtils.getLongFromIsoString( val.getDrilldownStartDateAsString(), null));	
				
				map.put(date, val);
			}
			else	{
				Integer newVal	=	dbEntry.getTE3();
				if (newVal == null)		{
					theLog.error( "NULL TE3 "+ dbEntry.getTE0());
					continue;
				}
				val.setValue( val.getValue() + newVal);
//				map.put(date, val);
			}
			
		}
		
		SortedSet<String> keys = new TreeSet<>(map.keySet());
		for (String key : keys) { 
			SampleEntry smpl = map.get(key);
			generateAndSetDescriptionPerDay( smpl);
			lstRet.add( smpl);
		}
		
		return lstRet;
		
	}
	
	public List<SampleEntry> aggregateDataPerMonth( List<DbEntry> lstDbEntries)
	{
		List<SampleEntry> lstRet	=	new ArrayList<SampleEntry>();
		Map<String, SampleEntry> map =	new HashMap<String, SampleEntry>();
		for ( DbEntry dbEntry : lstDbEntries)	{
			String szYearAndMonth	=	dbEntry.getTE0().substring(0, 7);
			String szYearAndMonthAndDay	=	dbEntry.getTE0().substring(0, 10);
			SampleEntry val	=	map.get(szYearAndMonth);
			if ( val == null)	{
				val	=	new SampleEntry();
				val.setValue( dbEntry.getTE3());
				
				// Label must be of type "May 2018"
				val.setLabel( TimeUtils.convertDateFromYYYYMMDDtoMMMYYYY( dbEntry.getTE0().substring(0, 11))); 
				val.setCause( dbEntry.getHC0E1());
				
				// Date must be of type "2018-05-01"
				val.setDate( TimeUtils.generateAngloSaxonDate(dbEntry.getTE0().substring(0, 7) + "-01"));
				
				// Drill Down End date must be of type "2018-05-31 23:59:59"
				val.setDrilldownEndDateAsString(TimeUtils.generateIsoDateEndOfMonth(szYearAndMonthAndDay));
				// Drill Down Start date must be of type "2018-05-01 00:00:00"
				val.setDrilldownStartDateAsString(TimeUtils.generateIsoDateStartOfMonth(szYearAndMonthAndDay));
				
				val.setDrilldownEndDate( TimeUtils.getLongFromIsoString( val.getDrilldownEndDateAsString(), null));
				val.setDrilldownStartDate( TimeUtils.getLongFromIsoString( val.getDrilldownStartDateAsString(), null));	

				
				map.put(szYearAndMonth, val);
			}
			else	{
				Integer newVal	=	dbEntry.getTE3();
				if (newVal == null)		{
					theLog.error( "NULL TE3 "+ dbEntry.getTE0());
					continue;
				}
				val.setValue( val.getValue() + newVal);
			}
			
		}
		
		SortedSet<String> keys = new TreeSet<>(map.keySet());
		for (String key : keys) { 
			SampleEntry smpl = map.get(key);
			generateAndSetDescriptionPerMonth( smpl);
			lstRet.add( smpl);
		}		
		return lstRet;		
	}
	
	public List<SampleEntry> aggregateDataPerQuarter( List<DbEntry> lstDbEntries)
	{
		List<SampleEntry> lstRet	=	new ArrayList<SampleEntry>();
		Map<String, SampleEntry> map =	new HashMap<String, SampleEntry>();
		for ( DbEntry dbEntry : lstDbEntries)	{
			String szYearAndMonth	=	dbEntry.getTE0().substring(0, 7);
			String szYearAndMonthAndDay	=	dbEntry.getTE0().substring(0, 10);
			String quarter	=	TimeUtils.getQuarterFromDate(szYearAndMonthAndDay);
			SampleEntry val	=	map.get(quarter);
			if ( val == null)	{
				val	=	new SampleEntry();
				val.setValue( dbEntry.getTE3());
				
				
				// Label must be of type "Quarter 1, 2018"
				val.setLabel( quarter); 
				val.setCause( dbEntry.getHC0E1());
				
				// Date must be of type "01/01/2018", "04/01/2018"
				val.setDate( TimeUtils.generateAngloSaxonDateStartOfQuarterFromDateYYYYMMDD(szYearAndMonthAndDay));
				
				// Drill Down End date must be of type "2018-05-31 23:59:59"
				val.setDrilldownEndDateAsString(TimeUtils.generateIsoDateEndOfQuarter(szYearAndMonthAndDay));
				// Drill Down Start date must be of type "2018-05-01 00:00:00"
				val.setDrilldownStartDateAsString(TimeUtils.generateIsoDateStartOfQuarter(szYearAndMonthAndDay));
				
				val.setDrilldownEndDate( TimeUtils.getLongFromIsoString( val.getDrilldownEndDateAsString(), null));
				val.setDrilldownStartDate( TimeUtils.getLongFromIsoString( val.getDrilldownStartDateAsString(), null));	

				theLog.info( "aggregateDataPerQuarter NEW PUT quarter=" + quarter + " val=" + val);
				
				map.put(quarter, val);
			}
			else	{
				Integer newVal	=	dbEntry.getTE3();
				if (newVal == null)		{
					theLog.error( "NULL TE3 "+ dbEntry.getTE0());
					continue;
				}
				val.setValue( val.getValue() + newVal);
			}
			
		}
		
		SortedSet<String> keys = new TreeSet<>(map.keySet());
		for (String key : keys) { 
			SampleEntry smpl = map.get(key);
			generateAndSetDescriptionPerMonth( smpl);
			lstRet.add( smpl);
		}		
		return lstRet;		
	}
	
	
	public List<SampleEntry> aggregateDataPerYear( List<DbEntry> lstDbEntries)
	{
		List<SampleEntry> lstRet	=	new ArrayList<SampleEntry>();
		Map<String, SampleEntry> map =	new HashMap<String, SampleEntry>();
		for ( DbEntry dbEntry : lstDbEntries)	{
			String szYear	=	dbEntry.getTE0().substring(0, 4);
			SampleEntry val	=	map.get(szYear);
			if ( val == null)	{
				val	=	new SampleEntry();
				val.setValue( dbEntry.getTE3());
				val.setLabel( szYear);
				val.setCause( dbEntry.getHC0E1());
				val.setDate( "01/01/" + szYear);
				val.setDrilldownEndDateAsString(TimeUtils.generateIsoDateEndOfYear(szYear));
				val.setDrilldownStartDateAsString(TimeUtils.generateIsoDateStartOfYear(szYear));
				
				
				val.setDrilldownEndDate( TimeUtils.getLongFromIsoString( val.getDrilldownEndDateAsString(), null));
				val.setDrilldownStartDate( TimeUtils.getLongFromIsoString( val.getDrilldownStartDateAsString(), null));	
				
				map.put(szYear, val);
			}
			else	{
				Integer newVal	=	dbEntry.getTE3();
				if (newVal == null)		{
					theLog.error( "NULL TE3 "+ dbEntry.getTE0());
					continue;
				}
				val.setValue( val.getValue() + newVal);
			}
			
		}
		
		SortedSet<String> keys = new TreeSet<>(map.keySet());
		for (String key : keys) { 
			SampleEntry smpl = map.get(key);
			generateAndSetDescriptionPerYear( smpl);
			lstRet.add( smpl);
		}
		
		return lstRet;
		
	}	
	
	private void generateAndSetDescriptionPer5Mins( SampleEntry smpl)
	{
		String sz	=	smpl.getDate() + "\n" + "Cause: " + smpl.getCause() + "\n" + 
					"Number of Errors: " + smpl.getValue();
		smpl.setDescription(sz);
	}
	private void generateAndSetDescriptionPerHour( SampleEntry smpl)
	{
		String sz	=	smpl.getDate() + "\n" + "Cause: " + smpl.getCause() + "\n" + 
					"Number of Errors: " + smpl.getValue();
		smpl.setDescription(sz);
	}
	private void generateAndSetDescriptionPerDay( SampleEntry smpl)
	{
		String sz	=	smpl.getDate() + " 00:00" + "\n" + "Cause: " + smpl.getCause() + "\n" + 
					"Number of Errors: " + smpl.getValue();
		smpl.setDescription(sz);
	}
	
	private void generateAndSetDescriptionPerYear( SampleEntry smpl)
	{
		String sz	=	smpl.getDate() + " 00:00" + "\n" + "Cause: " + smpl.getCause() + "\n" + 
					"Number of Errors: " + smpl.getValue();
		smpl.setDescription(sz);
	}
	
	private void generateAndSetDescriptionPerMonth( SampleEntry smpl)
	{
		String sz	=	smpl.getDate() + " 00:00" + "\n" + "Cause: " + smpl.getCause() + "\n" + 
					"Number of Errors: " + smpl.getValue();
		smpl.setDescription(sz);
	}

}
