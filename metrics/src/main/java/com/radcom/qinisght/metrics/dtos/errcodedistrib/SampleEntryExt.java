package com.radcom.qinisght.metrics.dtos.errcodedistrib;

import java.util.ArrayList;
import java.util.List;

public class SampleEntryExt {
	
	private String label;

	List<SeriesEntry> values;
	
	private String drilldownStartDateAsString;
	private String drilldownEndDateAsString;


	private String date;
	
	private Long drilldownStartDate;
	private Long drilldownEndDate;
	
	
	
	
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}

	public String getDrilldownStartDateAsString() {
		return drilldownStartDateAsString;
	}
	public void setDrilldownStartDateAsString(String drilldownStartDate) {
		this.drilldownStartDateAsString = drilldownStartDate;
	}
	public String getDrilldownEndDateAsString() {
		return drilldownEndDateAsString;
	}
	public void setDrilldownEndDateAsString(String drilldownEndDate) {
		this.drilldownEndDateAsString = drilldownEndDate;
	}

	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public Long getDrilldownStartDate() {
		return drilldownStartDate;
	}
	public void setDrilldownStartDate(Long drilldownStartDate) {
		this.drilldownStartDate = drilldownStartDate;
	}
	public Long getDrilldownEndDate() {
		return drilldownEndDate;
	}
	public void setDrilldownEndDate(Long drilldownEndDate) {
		this.drilldownEndDate = drilldownEndDate;
	}
	public List<SeriesEntry> getValues() {
		return values;
	}
	public void setValues(List<SeriesEntry> values) {
		this.values = values;
	}
	
	
	public void addSeriesEntry( SeriesEntry ent)
	{
		if (values == null)	{
			values	=	new ArrayList<SeriesEntry>();
		}
		values.add(ent);
	}

}
