package com.radcom.qinisght.metrics.business;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.radcom.qinisght.metrics.dtos.errcodedistrib.SampleEntryExt;
import com.radcom.qinisght.metrics.dtos.errcodedistrib.SeriesEntry;

public class SampleEntryAggregator {
	
	
	public void aggregateDataBySum( SampleEntryExt dateSampleEntry)
	{
		List<SeriesEntry> lst	=	dateSampleEntry.getValues();
		
		List<SeriesEntry> newList	=	new ArrayList<SeriesEntry>();
		Map<String, Integer> map	=	new HashMap<String, Integer>();
		for ( SeriesEntry seriesEntry : lst)	{
			String cause	=	seriesEntry.getCause();
			if (cause == null)
				continue;
			
			Integer oldVal	=	map.get(cause);	
			if ( oldVal == null)	{
				Integer newVal	=	new Integer( seriesEntry.getValue());
				map.put(cause, newVal);
			}
			else	{
				oldVal += seriesEntry.getValue();
				map.put(cause, oldVal);
				
			}
		}
		
		Set<String> theKeys	=	map.keySet();
		for ( String cause : theKeys)	{
			newList.add( new SeriesEntry( map.get(cause), cause));
		}
		
		
		//FIxed order of values 
		Collections.sort( newList, new ComparatorSeriesEntryFixedOrder());
		dateSampleEntry.setValues(newList);
	}

}
