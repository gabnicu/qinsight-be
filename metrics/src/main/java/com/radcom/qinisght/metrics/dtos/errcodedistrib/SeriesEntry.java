package com.radcom.qinisght.metrics.dtos.errcodedistrib;

public class SeriesEntry {
	
	private int value;
	private String cause;
	
	public SeriesEntry()
	{
		value	=	0;
		cause	=	null;
	}
	
	public SeriesEntry( int theVal, String theCause)
	{
		value	=	theVal;
		cause	=	theCause;
	}
	
	
	
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
	public String getCause() {
		return cause;
	}
	public void setCause(String cause) {
		this.cause = cause;
	}

	
	
	

}
