package com.radcom.qinisght.metrics.startup;

import java.util.Collection;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;
import javax.inject.Inject;

import org.apache.log4j.Logger;

import com.radcom.qinisght.metrics.services.ServiceNetworkElements;
import com.radcom.qinisght.metrics.services.nes.NetworkElementsResolver;

@Startup
@Singleton
public class EjbMetricsStartUpExt {
	

	private static final Logger theLog = Logger.getLogger(EjbMetricsStartUpExt.class);
	
    @Resource
    TimerService timerService;
	@Inject 
	NetworkElementsResolver resolver;

    @PostConstruct
    void init()  {
    	theLog.info("EjbMetricsStartUpExt init()");
    	setTheTimer( 10000);		
    }
    
    @PreDestroy
    void drawDown()
    {
		theLog.info("EjbMetricsStartUp drawDown()" );	
    }
    

    private void execFunction()
    {
    	resolver.initiateNetworkElementsLoad();
    }
	public void setTheTimer(long intervalDuration) {
		TimerConfig conf	=	new TimerConfig();
		conf.setPersistent( false);
		Timer timer = timerService.createIntervalTimer( 0, intervalDuration, conf);
    }
    
    @Timeout
    public void programmaticTimeout(Timer timer) 
    {
    	theLog.info("********************************** E X T Programmatic timeout occurred.");
        execFunction();
    }
}
