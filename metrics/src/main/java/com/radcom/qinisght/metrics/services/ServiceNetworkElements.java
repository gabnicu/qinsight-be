package com.radcom.qinisght.metrics.services;

import javax.inject.Inject;

import org.apache.log4j.Logger;

import com.radcom.qinisght.metrics.services.nes.NetworkElementsResolver;
import com.radcom.qinisght.metrics.startup.MetricsStartUpEngine;

public class ServiceNetworkElements implements Runnable
{
	private static final Logger theLog = Logger.getLogger(MetricsStartUpEngine.class);

	
	@Inject 
	NetworkElementsResolver resolver;
	
	@Override
	public void run() {
		theLog.info("777777777777777777777777777777777777777777777777777777777777 Scheduler called");
		resolver.incrementNumber();
	}

}
