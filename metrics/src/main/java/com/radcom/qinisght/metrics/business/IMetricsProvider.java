package com.radcom.qinisght.metrics.business;

import java.util.List;

import javax.enterprise.context.SessionScoped;

import com.radcom.qinisght.metrics.dtos.errcodedistrib.SampleEntry;

public interface IMetricsProvider {
	public List<SampleEntry> getMetrics( String startDate, String EndDate, int deltaUTC, String nes);
}
