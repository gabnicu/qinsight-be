package com.radcom.qinisght.metrics.business;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;

import org.apache.log4j.Logger;

import com.radcom.qinisght.metrics.dtos.errcodedistrib.SampleEntry;
import com.radcom.qinisght.metrics.dtos.errcodedistrib.SampleEntryExt;
import com.radcom.qinisght.metrics.model.diameter.errcodedistrib.DbEntry;
import com.radcom.qinisght.metrics.model.diameter.errcodedistrib.dao.DaoErrorCodeDistribution;
import com.radcom.qinisght.metrics.model.diameter.errcodedistrib.dao.impl.ProviderVertica;


@RequestScoped
public class MetricsProviderExt{
	
	
	private static final Logger theLog = Logger.getLogger( MetricsProviderExt.class);
	
	@Inject
	@ProviderVertica
	DaoErrorCodeDistribution daoVertica;
	
	
	
	@Inject 
	DataAggregatorExt aggregExt;
	
	public List<SampleEntryExt> getMetricsExt( String startDate, String endDate, int deltaUTC, String nes)
	{
		startDate	=	processDate( startDate);
		endDate	=	processDate( endDate);
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		boolean isMoreThan2days	=	false;
		AggregationType aggType		=	AggregationType.AGG_TYPE_PER_HOUR;
		try {
			Date dtStart	=	sdf.parse(startDate);
			Date dtEnd	=	sdf.parse(endDate);
			
			long delta	=	dtEnd.getTime() - dtStart.getTime();
			theLog.info( "delta time: " + delta);
			if ( delta < (3600L + 10) * 1000L)	{
				isMoreThan2days	=	false;
				aggType		=	AggregationType.AGG_TYPE_PER_5MINS;
			}
			else if ( delta < 2 * 24 * 3600L * 1000L)	{
				isMoreThan2days	=	false;
				aggType		=	AggregationType.AGG_TYPE_PER_HOUR;
			}
			else	{
				isMoreThan2days	=	true;
				if ( delta < 60 * 24 * 3600L * 1000L)
					aggType		=	AggregationType.AGG_TYPE_PER_DAY;
				else if ( delta < 6 * 31 * 24 * 3600L * 1000L)
					aggType		=	AggregationType.AGG_TYPE_PER_MONTH;
				else if ( delta < 367 * 24 * 3600L * 1000L)
					aggType		=	AggregationType.AGG_TYPE_PER_QUARTER;
				else
					aggType		=	AggregationType.AGG_TYPE_PER_YEAR;
			}
			theLog.info( "isMoreThan2days: " + isMoreThan2days + " aggType: " + aggType);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		
		List<DbEntry> lstDbEntries	=	daoVertica.getData(startDate, endDate, deltaUTC, nes, isMoreThan2days);
		return aggregExt.aggregateData(lstDbEntries, aggType);
	}
	
	// 2018-05-27 00:00:02
	private String processDate( String initialString)
	{
		
		if ( initialString.length() < 19)
			return null;
		if ( initialString.length() > 19)
			initialString	=	initialString.substring(0, 19);
		String sz	=	initialString.replaceAll("T", " ");
		return sz;			
	}
	

}
