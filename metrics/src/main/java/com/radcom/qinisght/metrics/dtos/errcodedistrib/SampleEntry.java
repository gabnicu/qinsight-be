package com.radcom.qinisght.metrics.dtos.errcodedistrib;

public class SampleEntry {
	
	private String label;
	private double value;
	public String cause;
	
	
	private String drilldownStartDateAsString;
	private String drilldownEndDateAsString;

	private String description;
	private String date;
	
	private Long drilldownStartDate;
	private Long drilldownEndDate;
	
	
	
	
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public double getValue() {
		return value;
	}
	public void setValue(double value) {
		this.value = value;
	}
	public String getCause() {
		return cause;
	}
	public void setCause(String cause) {
		this.cause = cause;
	}
	public String getDrilldownStartDateAsString() {
		return drilldownStartDateAsString;
	}
	public void setDrilldownStartDateAsString(String drilldownStartDate) {
		this.drilldownStartDateAsString = drilldownStartDate;
	}
	public String getDrilldownEndDateAsString() {
		return drilldownEndDateAsString;
	}
	public void setDrilldownEndDateAsString(String drilldownEndDate) {
		this.drilldownEndDateAsString = drilldownEndDate;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public Long getDrilldownStartDate() {
		return drilldownStartDate;
	}
	public void setDrilldownStartDate(Long drilldownStartDate) {
		this.drilldownStartDate = drilldownStartDate;
	}
	public Long getDrilldownEndDate() {
		return drilldownEndDate;
	}
	public void setDrilldownEndDate(Long drilldownEndDate) {
		this.drilldownEndDate = drilldownEndDate;
	}
	

}
