package com.radcom.qinisght.metrics.rest.diameter;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.radcom.qinisght.metrics.business.MetricsProviderExt;
import com.radcom.qinisght.metrics.dtos.errcodedistrib.SampleEntry;
import com.radcom.qinisght.metrics.dtos.errcodedistrib.SampleEntryExt;
import com.radcom.qinisght.metrics.model.diameter.errcodedistrib.MetricsEntry;
import com.radcom.qinisght.metrics.model.diameter.errcodedistrib.accessors.EjbGetMetrics;
import com.radcom.qinisght.metrics.model.diameter.errcodedistrib.dao.DaoErrorCodeDistribution;
import com.radcom.qinisght.metrics.services.nes.NetworkElementsResolver;
import com.radcom.qinsight.utils.file.FileUtil;
import com.radcom.qinsight.utils.rest.dtos.BasicDTO;
import com.radcom.qinsight.utils.time.TimeUtils;

import javax.ws.rs.QueryParam;



@Transactional
@Path("metrics")
public class RestDiameterExt {
	
	private static final Logger theLog = Logger.getLogger(RestDiameterExt.class);

	
	@Inject 
	MetricsProviderExt metricsProviderExt;
	
	@Inject 
	NetworkElementsResolver resolver;

    
    @GET
    @Path("diameter/errorCodeDistribution")
    @Produces({"application/json"})
    public Response getMetricsDiameterErrorCodeDistribution( 
    		@QueryParam("startDate") final Long startDate, 
    		@QueryParam("endDate") final Long endDate, 
    		@QueryParam("networkElements") final String networkElements,
    		@QueryParam("delta") final Integer delta) 
    {

    	theLog.info( "------ getMetricsDiameterErrorCodeDistribution startDate=" + startDate + 
    			" endDate=" + endDate + " networkElements=" + networkElements + " delta=" + delta);

    	long startDateExt	=	startDate + 3 * 3600 * 1000L;
    	long endDateExt	=	endDate + 3 * 3600 * 1000L;
    	String stdt	=	TimeUtils.getIsoStringFromLong( startDateExt, TimeUtils.getGmtStringFromDeltaInMinutes( delta));
    	String endt	=	TimeUtils.getIsoStringFromLong( endDateExt, TimeUtils.getGmtStringFromDeltaInMinutes( delta));
    	String nes	=	processNetworkElements( networkElements);
    	
    	theLog.info( "------ getMetricsDiameterErrorCodeDistribution stdt=" + stdt + " endt=" + endt);
    	
    	List<SampleEntryExt> lst	=	metricsProviderExt.getMetricsExt( stdt, endt, delta, nes);
    	BasicDTO dto	=	new BasicDTO();
		dto.setData( lst);
		return Response.status(Response.Status.OK).entity(dto).build();    
    }
    
   
    private String processNetworkElements( String initialString)
    {
    	/**********************/
    	initialString	=	initialString.replaceAll("\\[", "");
    	initialString	=	initialString.replaceAll("\\]", "");
    	initialString	=	initialString.replaceAll("\\{", "");
    	initialString	=	initialString.replaceAll("\\}", "");
    	initialString	=	initialString.replaceAll("\"", "");
    	theLog.info( "After Processing before split initialString=" + initialString);

    	
    	String items[]	=	initialString.split(",");
    	Set<String> hashItems	=	new HashSet<String>();
    	for ( String itm : items)	{
    		itm	=	itm.trim();
    		List<String> lstResolved	=	resolver.resolveNetworkElement(itm);
    		hashItems.addAll(lstResolved);
    	}
    	
    	
    	
    	StringBuffer sBuff	=	new StringBuffer();
    	for ( String itm : hashItems)	{
    		itm	=	itm.trim();
    		if ( !(itm.startsWith("'")))	{
    			itm	=	"'" + itm;
    		}
    		if ( !(itm.endsWith("'")))	{
    			itm	=	itm + "'";
    		}
    		sBuff.append(itm);
    		sBuff.append(",");   		
    	}
    	
    	String szRet	=	sBuff.toString();
    	// REMOVE LAST COMMA
    	szRet = szRet.substring(0, szRet.length() - 1);
    	theLog.info( "------ processNetworkElements result=" + szRet);
    	return szRet;
    	/*********************/   	
//    	return "'AAA_1', 'AAA_2', 'AAA_3', 'AS_1', 'AS_2', 'AS_3', 'AS_5', 'AS_6', 'AS_7', 'AS_8', 'AS_9', 'CDF', 'CSCF_1', 'CSCF_2', 'HSS_2', 'HSS_3', 'MMEcCC', 'Other', 'PCRF_1', 'PCRF_2', 'PGW_1', 'PGW_2', 'PGW_20', 'PGW_21', 'PRC10', 'PRC11', 'PRC2', 'PRC4', 'PRC5', 'PRC6', 'PRC7', 'PRC8', 'PRC9', 'SCEF001', 'SGW001', 'Unknown_3', 'Unknown_4', 'yoni1', 'yoni1_dns', 'yoni10', 'yoni11', 'yoni12', 'yoni13', 'yoni14', 'yoni15', 'yoni16', 'yoni17', 'yoni18', 'yoni2', 'yoni2_dns', 'yoni3', 'yoni4', 'yoni5', 'yoni6', 'yoni7', 'yoni8', 'yoni9'";
    }
   
}
