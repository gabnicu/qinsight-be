package com.radcom.qinisght.metrics.business;

import java.util.Comparator;

import com.radcom.qinisght.metrics.dtos.errcodedistrib.SeriesEntry;

public class ComparatorSeriesEntryFixedOrder implements Comparator<SeriesEntry>
{

	// order is 5140, 5002, 5420, 3004
	
	@Override
	public int compare(SeriesEntry o1, SeriesEntry o2) {	
		String sz1	=	o1.getCause();
		String sz2	=	o2.getCause();
		if ( sz1.contains("5140 EXP ERROR INITIAL PARAMETERS"))
			return -1;
		else if ( sz1.contains("5002 UNKNOWN SESSION ID"))	{
			if ( sz2.contains("5140 EXP ERROR INITIAL PARAMETERS"))
				return 1;
			return -1;
		}
		else if ( sz1.contains("5420 EXP ERROR UNKNOWN EPS SUBSCRIPTION"))	{
			if ( sz2.contains("5140 EXP ERROR INITIAL PARAMETERS"))
				return 1;
			if ( sz2.contains("5002 UNKNOWN SESSION ID"))
				return 1;
			return -1;
		}
		else if ( sz1.contains("3004 TOO BUSY"))
			return 1;
		
		return 0;

	}

}
