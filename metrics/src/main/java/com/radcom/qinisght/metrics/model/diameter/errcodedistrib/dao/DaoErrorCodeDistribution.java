package com.radcom.qinisght.metrics.model.diameter.errcodedistrib.dao;

import java.util.List;

import com.radcom.qinisght.metrics.model.diameter.errcodedistrib.DbEntry;

public interface DaoErrorCodeDistribution {
	public List<DbEntry> getData( String startDate, String EndDate, int deltaUTC, String nes, boolean isMoreThan2days);

}
