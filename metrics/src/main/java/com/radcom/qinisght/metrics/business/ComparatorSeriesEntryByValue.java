package com.radcom.qinisght.metrics.business;

import java.util.Comparator;

import com.radcom.qinisght.metrics.dtos.errcodedistrib.SeriesEntry;

public class ComparatorSeriesEntryByValue implements Comparator<SeriesEntry>
{

	@Override
	public int compare(SeriesEntry o1, SeriesEntry o2) {		
		return (new Integer(o1.getValue())).compareTo(new Integer(o2.getValue()));
	}

}
