package com.radcom.qinisght.metrics.startup;

import java.util.Collection;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerService;
import javax.inject.Inject;

import org.apache.log4j.Logger;

import com.radcom.qinisght.metrics.services.ServiceNetworkElements;
import com.radcom.qinisght.metrics.services.nes.NetworkElementsResolver;

@Startup
@Singleton
public class EjbMetricsStartUp {
	
	private static final String TIMER_NAME	=	"TIMER_NAME_NES_FROM_METRICS";
	
	private static final Logger theLog = Logger.getLogger(EjbMetricsStartUp.class);
//	private ScheduledExecutorService sched;
	
	
    @Resource
    TimerService timerService;
	@Inject 
	NetworkElementsResolver resolver;

    @PostConstruct
    void init()  {
    	theLog.info("EjbMetricsStartUp init()");
//		sched = Executors.newSingleThreadScheduledExecutor();
//        sched.scheduleWithFixedDelay(new ServiceNetworkElements(), 0, 10, TimeUnit.SECONDS);
		listTimersAndCancelPersisted();
//		execAndRestartTimer();
    }
    
    @PreDestroy
    void drawDown()
    {
		theLog.info("EjbMetricsStartUp drawDown()" );	
//		sched.shutdownNow();
    }
    
    private void listTimersAndCancelPersisted()
    {
	
    	Collection<Timer> lstTimers	=	timerService.getTimers();
    	if ( lstTimers == null)
    		return;
    	for ( Timer tim : lstTimers)	{
    		if ( TIMER_NAME.equals( tim.getInfo()))	{
    			tim.cancel();
    			continue;
    		}
    	}
    }
    private void execAndRestartTimer()
    {
    	resolver.incrementNumber();
		setTheTimer( 5 * 1000);
    }
	public void setTheTimer(long intervalDuration) {
        Timer timer = timerService.createTimer(intervalDuration, TIMER_NAME);
    }
    
    @Timeout
    public void programmaticTimeout(Timer timer) 
    {
    	theLog.info("----------------------------------- Programmatic timeout occurred.");
    	timer.cancel();
        execAndRestartTimer();
    }
}
