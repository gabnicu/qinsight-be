package com.radcom.qinsight.templates.rest.response;

import java.util.ArrayList;
import java.util.List;


import com.radcom.qinsight.templates.model.DashboardTemplate;

public class QueryResponseDashboardTemplates {
	
	private List<DashboardTemplate> data	=	null;
	
	public void addEntry( DashboardTemplate ent)
	{
		if ( ent == null)
			return;
		if ( data == null)
			data	=	new ArrayList<DashboardTemplate>();
		data.add(ent);
	}

	public List<DashboardTemplate> getData() {
		return data;
	}


	
}
