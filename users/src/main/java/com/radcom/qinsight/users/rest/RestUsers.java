package com.radcom.qinsight.users.rest;

import java.util.List;

import javax.ejb.EJB;
import javax.transaction.Transactional;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import com.radcom.qinsight.users.business.EjbUsersBusiness;
import com.radcom.qinsight.users.model.DashboardExt;
import com.radcom.qinsight.users.model.User;
import com.radcom.qinsight.users.model.accessors.ManagerUsers;
import com.radcom.qinsight.users.rest.response.QueryResponseUsers;
import com.radcom.qinsight.utils.rest.dtos.BasicDTO;



@Transactional
@Path("users")
public class RestUsers {
	
	
	@EJB
	ManagerUsers mgr;
	
	@EJB
	EjbUsersBusiness usrBusiness;
	
	
//    @GET
//    @Produces({"application/json"})
//    public QueryResponseDashboardTemplates findAll( ) {
//    	return mgr.getAll();
//    }
    @GET
    @Produces({"application/json"})
    public Response findAll( @QueryParam("name") String name) {
    	if ( name == null)	{
    		System.out.println("RestUsers GET name=null");
    		return Response.status(Response.Status.OK).entity(mgr.getAll()).build();
    	}
    	System.out.println("RestUsers GET name=" + name);
    	List<User> lst	=	mgr.getUserByUsername(name);
    	if ( lst.size() == 0)
    		return Response.status(Response.Status.NOT_FOUND).build();
    	return Response.status(Response.Status.OK).entity( lst).build();
    }
    
    @GET
    @Path("{id}")
    @Produces({"application/json"})
    public Response findById(@PathParam("id") String id) {
    	
    	if ( id == null)	{
    		System.out.println("RestUsers GET id=null");
    		QueryResponseUsers resp	=	mgr.getAll();
        	
        	return Response.status(Response.Status.OK).entity( resp).build();
    		
    	}
    	User usr	=	mgr.getById(id);
    	if (usr == null)	{
    		System.out.println("----------------------------------NOT FOUND");
    		return Response.status(Response.Status.NOT_FOUND).build();
    	}
    	System.out.println("----------------------------------Has user entity");
    	return Response.status(Response.Status.OK).entity(usr).build();
    }
    @GET
    @Path("{id}/dashboards")
    @Produces({"application/json"})
    public Response getAllDashboardsForUser(@PathParam("id") String id) {
    	
    	return usrBusiness.getAllDashboardsForUserId( id);
    }
    @GET
    @Path("{id}/dashboards/{dashboardId}")
    @Produces({"application/json"})
    public Response getDashboardByIdForUser(@PathParam("id") String id, @PathParam("dashboardId") String dashboardId) {
    	
    	return usrBusiness.getDashboardByIdForUserId(id, dashboardId);
    }

    @GET
    @Path("{id}/dashboards/recent")
    @Produces({"application/json"})
    public Response getLastViewedDashboardsForUser(@PathParam("id") String id) {
    	
    	return usrBusiness.getLatestViewedDashboardsForUserId( id);
    }

    
    @DELETE
    @Path("{id}")
    @Produces({"application/json"})
    public Response removeUser(@PathParam("id") String id)
    {
    	System.out.println("RestUsers removeUser, id = " + id);
    	boolean bret	=	mgr.delete( id);
    	if ( bret == false)
    		return Response.status(Response.Status.NOT_FOUND).build();
    	else
    		return Response.status(Response.Status.OK).build();
    		
    }

    @POST
    @Consumes({"application/json"})
    @Produces({"application/json"})
    public Response addUser( User usr)
    {
    	String id	=	mgr.save( usr);
    	return Response.status(Response.Status.OK).entity(id).build();   		
    }
    
    
    @PUT
    @Path("{id}/dashboards/{dashboardId}")
    @Consumes({"application/json"})
    public Response modify( User usr)
    {
    	boolean bret	=	mgr.addDashboardsToUser( usr);
    	if ( bret == false)
    		return Response.status(Response.Status.NOT_FOUND).build();
    	else
    		return Response.status(Response.Status.OK).build();
    }

//    @PUT
//    @Path("{userId}/dashboards/{dashboardId}/lastDisplayed")
//    @Consumes({"application/json"})
//    public Response modify( @PathParam("userId") String userId, @PathParam("dashboardId") String dashboardId, BasicDTO dto)
//    {
//    	return usrBusiness.updateLastDisplayedForDashboard(userId, dashboardId, dto);
//    }

    @POST
    @Path("{userId}/dashboards/{dashboardId}/lastDisplayed")
    @Consumes({"application/json"})
    @Produces({"application/json"})
    public Response modify( @PathParam("userId") String userId, @PathParam("dashboardId") String dashboardId)
    {
    	return usrBusiness.updateLastDisplayedForDashboard(userId, dashboardId);
    }
    
    @POST
    @Path("{userId}/dashboards")
    @Consumes({"application/json"})
    public Response addDashboardToUser( @PathParam("userId") String userId, DashboardExt dto)
    {

    	return usrBusiness.addDashboardToUser(userId, dto);
    }

    @PUT
    @Path("{userId}/dashboards")
    @Consumes({"application/json"})
    public Response editDashboardForUser( @PathParam("userId") String userId, DashboardExt dto)
    {

    	return usrBusiness.editDashboardForUser(userId, dto);
    }

    
    
    
    
}
