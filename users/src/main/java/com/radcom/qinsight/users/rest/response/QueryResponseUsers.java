                          package com.radcom.qinsight.users.rest.response;

import java.util.ArrayList;
import java.util.List;

import com.radcom.qinsight.users.model.User;

public class QueryResponseUsers {
	
	private List<User> data	=	null;
	
	public void addEntry( User ent)
	{
		if ( ent == null)
			return;
		if ( data == null)
			data	=	new ArrayList<User>();
		data.add(ent);
	}

	public List<User> getData() {
		return data;
	}


	
}
