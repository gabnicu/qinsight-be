package com.radcom.qinsight.users.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Embeddable
public class Dashlet {

	
	@Column(unique=true)
	private String id;

	@Enumerated(EnumType.STRING)
	private DashletType type;
	
//    @Temporal(TemporalType.TIMESTAMP)
//    private Date startDate;
//    
//    @Temporal(TemporalType.TIMESTAMP)
//    private Date endDate;
    
    private Long startDate;
    private Long endDate;
    
    private String networkElements;
    
    private Integer columnPosition;
    private Integer rowPosition;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public DashletType getType() {
		return type;
	}

	public void setType(DashletType type) {
		this.type = type;
	}


	public String getNetworkElements() {
		return networkElements;
	}

	public void setNetworkElements(String networkElements) {
		this.networkElements = networkElements;
	}

	public Integer getColumnPosition() {
		return columnPosition;
	}

	public void setColumnPosition(Integer columnPosition) {
		this.columnPosition = columnPosition;
	}

	public Integer getRowPosition() {
		return rowPosition;
	}

	public void setRowPosition(Integer rowPosition) {
		this.rowPosition = rowPosition;
	}

	public Long getStartDate() {
		return startDate;
	}

	public void setStartDate(Long startDate) {
		this.startDate = startDate;
	}

	public Long getEndDate() {
		return endDate;
	}

	public void setEndDate(Long endDate) {
		this.endDate = endDate;
	}	
	
	
	
}
