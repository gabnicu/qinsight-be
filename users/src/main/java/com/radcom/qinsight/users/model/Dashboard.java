package com.radcom.qinsight.users.model;


import java.util.List;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.FetchType;



@Embeddable
public class Dashboard {
	
	@Column(unique=true)
	private String id;
	
	private String name;
	private Integer numRows;
	private Integer numColumns;
	

	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getNumRows() {
		return numRows;
	}
	public void setNumRows(Integer numRows) {
		this.numRows = numRows;
	}
	public Integer getNumColumns() {
		return numColumns;
	}
	public void setNumColumns(Integer numColumns) {
		this.numColumns = numColumns;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	


}
