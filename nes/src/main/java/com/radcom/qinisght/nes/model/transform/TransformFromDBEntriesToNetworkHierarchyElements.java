package com.radcom.qinisght.nes.model.transform;

import java.util.List;

import com.radcom.qinisght.nes.model.NetworkElementEntry;
import com.radcom.qinisght.nes.model.NetworkHierarchyElement;

public interface TransformFromDBEntriesToNetworkHierarchyElements {
	
	public NetworkHierarchyElement transformFromDbRepresentation( List<NetworkElementEntry> lst, boolean recursive);
	public List<NetworkHierarchyElement> transformAsListFromDbRepresentation( List<NetworkElementEntry> lst, boolean recursive);

}
