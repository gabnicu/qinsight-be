package com.radcom.qinisght.nes.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.SessionScoped;
import javax.enterprise.inject.Any;
import javax.enterprise.inject.Default;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import com.radcom.qinisght.nes.model.NetworkElementEntry;
import com.radcom.qinisght.nes.model.NetworkHierarchyElement;
import com.radcom.qinisght.nes.model.accessors.EjbNetworkElementsAccesor;
import com.radcom.qinisght.nes.model.accessors.EjbGetNetworkElements;
import com.radcom.qinisght.nes.model.transform.TransformFromDBEntriesToNetworkHierarchyElements;
import com.radcom.qinisght.nes.qualifs.ThreeLevelsNetworkHierarchy;
import com.radcom.qinsight.utils.rest.dtos.BasicDTO;
import com.radcom.qinsight.utils.rest.dtos.DTOWithInclusions;




@Stateless
@Path("nes")
public class RestNetworkElements {

	@EJB
	EjbNetworkElementsAccesor ejbLoader;
	

	
	@Inject
	TransformFromDBEntriesToNetworkHierarchyElements transformer;

//    @GET
//    @Produces({"application/json"})
//    public Response findAll( @QueryParam("path") String path,  @QueryParam("include") String include) {
//    	if ( path == null)	{		
//    		path	=	"/ALL";
//    	}
//
//		List<NetworkElementEntry> lst	=	ejbLoader.getTheList(); 
//		boolean recursive = false;
//		if ( "subLocations".equals( include))
//			recursive	=	true;
//		
//		NetworkHierarchyElement basicData	=	transformer.transformFromDbRepresentation(lst, false);
//		List<NetworkHierarchyElement> lstData	=	new ArrayList<NetworkHierarchyElement>();
//		lstData.add( basicData);
//		if ( recursive == false)	{
//			BasicDTO dto	=	new BasicDTO();
//			dto.setData( lstData);	
//			return Response.status(Response.Status.OK).entity(dto).build();
//		}
//		DTOWithInclusions dto	=	new DTOWithInclusions();
//		dto.setData(lstData);
//		NetworkHierarchyElement fullNHE	=	transformer.transformFromDbRepresentation(lst, true);
//		dto.setIncluded(fullNHE);
//    	return Response.status(Response.Status.OK).entity(dto).build();
//    	
//    }
	
	
	
    @GET
    @Produces({"application/json"})
    public Response findAll( @QueryParam("path") String path,  @QueryParam("include") String include) {
    	if ( path == null)	{		
    		path	=	"/ALL";
    	}

		List<NetworkElementEntry> lst	=	ejbLoader.getTheList(); 
		boolean recursive = false;
		if ( "subLocations".equals( include))
			recursive	=	true;
		
		List<NetworkHierarchyElement> lstData	=	transformer.transformAsListFromDbRepresentation(lst, true);
		recursive	=	true;
		if ( recursive == false)	{
			BasicDTO dto	=	new BasicDTO();
			dto.setData( lstData);	
			return Response.status(Response.Status.OK).entity(dto).build();
		}
		DTOWithInclusions dto	=	new DTOWithInclusions();
		dto.setData(lstData);
		NetworkHierarchyElement fullNHE	=	transformer.transformFromDbRepresentation(lst, true);
		dto.setIncluded(fullNHE);
    	return Response.status(Response.Status.OK).entity(dto).build();
    	
    }	
	
    @GET
    @Path("dbEntries")
    @Produces({"application/json"})
    public Response findAllDbEntries() {
		List<NetworkElementEntry> lstDbEntries	=	ejbLoader.getTheList(); 
		BasicDTO dto	=	new BasicDTO();
		dto.setData( lstDbEntries);	
		return Response.status(Response.Status.OK).entity(dto).build();   	
    }	
	
	
}
