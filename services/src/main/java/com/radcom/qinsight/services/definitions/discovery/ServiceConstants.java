package com.radcom.qinsight.services.definitions.discovery;

public class ServiceConstants {
	
	public static final String SERVICE_USERS										=	"users";
	public static final String SERVICE_NETWORK_ELEMENTS								=	"networkElements";
	public static final String SERVICE_NETWORK_ELEMENTS_DB_ENTRIES					=	"networkElements/dbEntries";
	public static final String SERVICE_DASHBOARD_TEMPLATES							=	"dashboardTemplates";
	public static final String SERVICE_DNS_ERROR_CODES								=	"dns/errorCodes";
	public static final String SERVICE_DNS_RESPONSE_TIME							=	"dns/responseTime";
	public static final String SERVICE_ADVANCED_FILTERS								=	"advfilt";
	public static final String SERVICE_METRICS_DIAMETER_ERROR_CODE_DISTRIBUTION		=	"metrics/diameter/errorCodeDistribution";
	
	public static final String ENV_VAR_SERVICE_USERS					=	"ENV_VAR_SERVICE_USERS";
	public static final String ENV_VAR_SERVICE_NETWORK_ELEMENTS			=	"ENV_VAR_SERVICE_NETWORK_ELEMENTS";
	public static final String ENV_VAR_SERVICE_NETWORK_ELEMENTS_DB_ENTRIES			=	"ENV_VAR_SERVICE_NETWORK_ELEMENTS_DB_ENTRIES";
	public static final String ENV_VAR_SERVICE_DASHBOARD_TEMPLATES		=	"ENV_VAR_SERVICE_DASHBOARD_TEMPLATES";
	public static final String ENV_VAR_SERVICE_DNS_ERROR_CODES			=	"ENV_VAR_SERVICE_DNS_ERROR_CODES";
	public static final String ENV_VAR_SERVICE_DNS_RESPONSE_TIME		=	"ENV_VAR_SERVICE_DNS_RESPONSE_TIME";
	public static final String ENV_VAR_SERVICE_METRICS_DIAMETER_ERROR_CODE_DISTRIBUTION					=	"ENV_VAR_SERVICE_METRICS_DIAMETER_ERROR_CODE_DISTRIBUTION";
	public static final String ENV_VAR_SERVICE_ADVANCED_FILTERS			=	"ENV_VAR_SERVICE_ADVANCED_FILTERS";
	

}
