package com.radcom.qinsight.services.impl;

import java.net.URI;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.UriBuilder;

import com.radcom.qinsight.services.definitions.discovery.ServiceDiscovery;

public abstract class AbsServiceDiscovery implements ServiceDiscovery {

	private WebTarget userService;
	private WebTarget networkElementsService;
	private WebTarget networkElementsDbEntriesService;
	private WebTarget dashboardTemplatesService;
	private WebTarget dnsErrorCodesService;
	private WebTarget dnsResponseTimeService;
	private WebTarget advancedFiltersService;
	private WebTarget metricsDiameterErrorCodeDistributionService;

	public abstract String getUserServiceURI();
	public abstract String getNetworkElementsServiceURI();
	public abstract String getNetworkElementsDbEntriesServiceURI();
	public abstract String getDashboardTemplatesServiceURI();
	public abstract String getDnsErrorCodesServiceURI();
	public abstract String getDnsResponseTimeURI();
	public abstract String getAdvancedFiltersURI();
	public abstract String getMetricsDiameterErrorCodeDistributionURI();

	@Override
	public WebTarget getUserService( String pathParam) {
		if ( pathParam == null)		{
			userService = ClientBuilder.newClient()
					.target(UriBuilder.fromUri(URI.create(getUserServiceURI())).path("/user/resources/user").build());
		}
		else		{
			userService = ClientBuilder.newClient()
					.target(UriBuilder.fromUri(URI.create(getUserServiceURI())).path("/user/resources/user/" + pathParam).build());
		}

		return userService;
	}

	@Override
	public WebTarget getDnsErrorCodesService() {
		if (null == dnsErrorCodesService) {
			dnsErrorCodesService = ClientBuilder.newClient().target(UriBuilder
					.fromUri(URI.create(getDnsErrorCodesServiceURI())).path("/dns/resources/dns/errorCodes").build());
		}

		return dnsErrorCodesService;
	}

	@Override
	public WebTarget getDnsResponseTimeService() {
		if (null == dnsResponseTimeService) {
			dnsResponseTimeService = ClientBuilder.newClient().target(UriBuilder
					.fromUri(URI.create(getDnsErrorCodesServiceURI())).path("/dns/resources/dns/responseTime").build());
		}

		return dnsResponseTimeService;
	}
	
	@Override
	public WebTarget getDashboardTemplatesService() {
		if (null == dashboardTemplatesService) {
			dashboardTemplatesService = ClientBuilder.newClient()
					.target(UriBuilder.fromUri(URI.create(getDashboardTemplatesServiceURI())).path("/dashboardTemplate/resources/dashboardTemplates").build());
		}
		return dashboardTemplatesService;
	}


	@Override
	public WebTarget getNetworkElementsService() {
		if (null == networkElementsService) {
			networkElementsService = ClientBuilder.newClient()
					.target(UriBuilder.fromUri(URI.create(getDashboardTemplatesServiceURI())).path("/nes/resources/nes").build());
		}
		return networkElementsService;
	}

	@Override
	public WebTarget getNetworkElementsDbEntriesService() {
		if (null == networkElementsDbEntriesService) {
			networkElementsDbEntriesService = ClientBuilder.newClient()
					.target(UriBuilder.fromUri(URI.create(getDashboardTemplatesServiceURI())).path("/nes/resources/nes/dbEntries").build());
		}
		return networkElementsDbEntriesService;
	}

	@Override
	public WebTarget getAdvancedFiltersService() {
		if (null == advancedFiltersService) {
			advancedFiltersService = ClientBuilder.newClient()
					.target(UriBuilder.fromUri(URI.create(getDashboardTemplatesServiceURI())).path("/advfilt/resources/advfilt").build());
		}
		return advancedFiltersService;
	}
	
	@Override
	public WebTarget getMetricsDiameterErrorCodeDistributionService()
	{
		if (null == metricsDiameterErrorCodeDistributionService) {
			metricsDiameterErrorCodeDistributionService = ClientBuilder.newClient()
					.target(UriBuilder.fromUri(URI.create(getDashboardTemplatesServiceURI())).path("/metrics/resources/metrics/diameter/errorCodeDistribution").build());
		}
		return metricsDiameterErrorCodeDistributionService;		
	}

}
