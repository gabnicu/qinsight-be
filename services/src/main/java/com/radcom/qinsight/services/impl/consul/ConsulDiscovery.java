package com.radcom.qinsight.services.impl.consul;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.client.WebTarget;

import com.radcom.qinsight.services.ConsulServices;
import com.radcom.qinsight.services.definitions.registration.ServiceRegistry;
import com.radcom.qinsight.services.impl.AbsServiceDiscovery;


@ConsulServices
@ApplicationScoped
public class ConsulDiscovery extends AbsServiceDiscovery {

    
    private ServiceRegistry services	=	new ConsulRegistry();

    @Override
    public String getUserServiceURI() {
        return services.discoverServiceURI("user");
    }

	@Override
	public String getDnsErrorCodesServiceURI() {
		return services.discoverServiceURI("dns/errorCodes");
	}

	@Override
	public String getDnsResponseTimeURI() {
		return services.discoverServiceURI("dns/responseTime");
	}

	@Override
	public String getDashboardTemplatesServiceURI() {
        return services.discoverServiceURI("dashboardTemplate");
	}

	@Override
	public String getNetworkElementsServiceURI() {
        return services.discoverServiceURI("nes");
	}

	@Override
	public String getNetworkElementsDbEntriesServiceURI() {
		return services.discoverServiceURI("nes/dbEntries");
	}

	@Override
	public String getAdvancedFiltersURI() {
		return services.discoverServiceURI("advfilt");
	}

	@Override
	public String getMetricsDiameterErrorCodeDistributionURI() {
		return services.discoverServiceURI("metricsDiameterErrorCodeDistribution");
	}


}
