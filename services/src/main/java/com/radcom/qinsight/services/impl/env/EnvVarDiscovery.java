package com.radcom.qinsight.services.impl.env;

import java.net.URI;

import javax.ejb.EJB;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.UriBuilder;

import org.apache.log4j.Logger;

import com.radcom.qinsight.services.EnvVarServices;
import com.radcom.qinsight.services.definitions.discovery.ServiceConstants;
import com.radcom.qinsight.services.definitions.discovery.ServiceDiscovery;
import com.radcom.qinsight.utils.WildFlyUtil;

@EnvVarServices
@ApplicationScoped
public class EnvVarDiscovery implements ServiceDiscovery
{
	private static final Logger theLog = Logger.getLogger(EnvVarDiscovery.class);
	
	private WebTarget usersService;
	private WebTarget networkElementsService;
	private WebTarget networkElementsDbEntriesService;
	private WebTarget dashboardTemplatesService;
	private WebTarget dnsErrorCodesService;
	private WebTarget dnsResponseTimeService;	
	private WebTarget advancedFiltersService;
	private WebTarget metricsDiameterErrorCodeDistributionService;
	
	private int roundRobinUsers					=	0;
	private int roundRobinDashboardTemplates	=	0;
	
	
	
	@Inject
	@EnvVarServices
	EnvVarRegistry serviceRegistry;
	
	@EJB
	WildFlyUtil util;
	

	@Override
	public WebTarget getUserService( String pathParam) {
		String sz	=	serviceRegistry.discoverServiceURI( ServiceConstants.SERVICE_USERS);
		if ( sz == null)	{
			theLog.error("getUserService() NULL from environment variable, resorting to local IP .........");
			sz	=	"http://" + util.getHostName()+ ":" + util.getHostPort();
			theLog.error("getUserService() sz=" + sz);
		}

		theLog.error("111111111111111111111111111111111111111 EnvVarDiscovery::getUserService() sz=" + sz);
		String[] arrIps	=	parseEnvVar(sz);
		if ( arrIps.length > 1)		{
			if ( roundRobinUsers >= arrIps.length)	
				roundRobinUsers	=	0;
			sz	=	"http://" + arrIps[roundRobinUsers];
			roundRobinUsers ++;
		}
		theLog.error("222222222222222222222222222222222222222 EnvVarDiscovery::getUserService() sz=" + sz);
		
		
//		if ( usersService == null)	{
//			usersService	=	getService( sz, "/users/resources/users");
//		}
		if ( pathParam == null)
			usersService	=	getService( sz, "/users/resources/users");
		else
			usersService	=	getService( sz, "/users/resources/users/" + pathParam);
		return usersService;
	}

	@Override
	public WebTarget getDnsErrorCodesService() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public WebTarget getDnsResponseTimeService() {
		// TODO Auto-generated method stub
		return null;
	}

	
	private WebTarget getService( String location, String path) {
		theLog.error("EnvVarDiscovery::getService() location=" + location + ", path=" + path);
		WebTarget webTarget = ClientBuilder.newClient().target(UriBuilder
					.fromUri(URI.create( location)).path(path).build());

		return webTarget;
	}

	@Override
	public WebTarget getDashboardTemplatesService() {
		String sz	=	serviceRegistry.discoverServiceURI( ServiceConstants.SERVICE_DASHBOARD_TEMPLATES);
		if ( sz == null)	{
			theLog.error("getDashboardTemplatesService() NULL from environment variable, getting to local IP .........");
			sz	=	"http://" + util.getHostName()+ ":" + util.getHostPort();
		}

		theLog.error("111111111111111111111111111111111111111 EnvVarDiscovery::getDashboardTemplatesService() sz=" + sz);
		String[] arrIps	=	parseEnvVar(sz);
		if ( arrIps.length > 1)		{
			if ( roundRobinDashboardTemplates >= arrIps.length)	
				roundRobinDashboardTemplates	=	0;
			sz	=	"http://" + arrIps[roundRobinDashboardTemplates];
			roundRobinDashboardTemplates ++;
		}
		theLog.error("222222222222222222222222222222222222222 EnvVarDiscovery::getDashboardTemplatesService() sz=" + sz);
		

		
		if ( dashboardTemplatesService == null)	{
			dashboardTemplatesService	=	getService( sz, "/dashboardTemplate/resources/dashboardTemplates");
		}
		return dashboardTemplatesService;
	}

	@Override
	public WebTarget getNetworkElementsService() {
		String sz	=	serviceRegistry.discoverServiceURI( ServiceConstants.SERVICE_NETWORK_ELEMENTS);
		if ( sz == null)	{
			theLog.error("getNetworkElementsService() NULL from environment variable, getting to local IP .........");
			sz	=	"http://" + util.getHostName()+ ":" + util.getHostPort();
		}
		
		if ( networkElementsService == null)	{
			networkElementsService	=	getService( sz, "/nes/resources/nes");
		}
		return networkElementsService;
	}
	
	@Override
	public WebTarget getNetworkElementsDbEntriesService() {
		String sz	=	serviceRegistry.discoverServiceURI( ServiceConstants.SERVICE_NETWORK_ELEMENTS_DB_ENTRIES);
		if ( sz == null)	{
			theLog.error("getNetworkElementsDbEntriesService() NULL from environment variable, getting to local IP .........");
			sz	=	"http://" + util.getHostName()+ ":" + util.getHostPort();
		}
		
		if ( networkElementsDbEntriesService == null)	{
			networkElementsDbEntriesService	=	getService( sz, "/nes/resources/nes/dbEntries");
		}
		return networkElementsDbEntriesService;
	}
	
	@Override
	public WebTarget getAdvancedFiltersService() {
		String sz	=	serviceRegistry.discoverServiceURI( ServiceConstants.ENV_VAR_SERVICE_ADVANCED_FILTERS);
		if ( sz == null)	{
			theLog.error("getAdvancedFiltersService() NULL from environment variable, resorting to local IP .........");
			sz	=	"http://" + util.getHostName()+ ":" + util.getHostPort();
		}
		
		if ( advancedFiltersService == null)	{
			advancedFiltersService	=	getService( sz, "/advfilt/resources/advfilt/procType");
		}
		return advancedFiltersService;
	}
	
	
	private String[] parseEnvVar( String sz)
	{
		return sz.split( ";");
	}

	@Override
	public WebTarget getMetricsDiameterErrorCodeDistributionService() {
		String sz	=	serviceRegistry.discoverServiceURI( ServiceConstants.ENV_VAR_SERVICE_METRICS_DIAMETER_ERROR_CODE_DISTRIBUTION);
		if ( sz == null)	{
			theLog.error("getMetricsDiameterErrorCodeDistributionService() NULL from environment variable, resorting to local IP .........");
			sz	=	"http://" + util.getHostName()+ ":" + util.getHostPort();
		}
		
		if ( metricsDiameterErrorCodeDistributionService == null)	{
			metricsDiameterErrorCodeDistributionService	=	getService( sz, "/metrics/resources/metrics/diameter/errorCodeDistribution");
		}
		return metricsDiameterErrorCodeDistributionService;
	}

}
