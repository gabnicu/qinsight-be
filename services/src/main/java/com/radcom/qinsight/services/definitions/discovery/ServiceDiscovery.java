package com.radcom.qinsight.services.definitions.discovery;

import javax.ws.rs.client.WebTarget;

public interface ServiceDiscovery {
	
	public WebTarget getUserService( String pathParam);
	public WebTarget getNetworkElementsService();
	public WebTarget getNetworkElementsDbEntriesService();
	public WebTarget getDnsErrorCodesService();
	public WebTarget getDnsResponseTimeService();
	public WebTarget getDashboardTemplatesService();
	
	public WebTarget getAdvancedFiltersService();
	public WebTarget getMetricsDiameterErrorCodeDistributionService();
	

}
