package org.javaee7.wildfly.samples.everest;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;

import com.radcom.qinsight.services.impl.consul.ConsulDiscovery;
import com.radcom.qinsight.services.definitions.discovery.ServiceDiscovery;
import com.radcom.qinsight.services.ConsulServices;


@ApplicationScoped
public class Resources {

    @Inject @ConsulServices
    ServiceDiscovery services;

    @Produces
    public ServiceDiscovery getService() {
        return services;
    }
}
