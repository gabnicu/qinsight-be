package com.radcom.qinsight.api.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import com.radcom.qinsight.services.impl.consul.ConsulDiscovery;
import com.radcom.qinsight.services.EnvVarServices;
import com.radcom.qinsight.services.definitions.discovery.ServiceDiscovery;

import com.radcom.qinsight.api.rest.tos.TransferObjDnsEntry;

@Stateless
@Path("metrics")
public class ApiGatewayMetrics {
	
	@Inject
	@EnvVarServices
    ServiceDiscovery serviceDiscovery;
     
 
//    @GET
//    @Path("diameter/errorCodeDistribution")
//    @Produces({"application/json"})
//    public Object getMetricsDiameterErrorCodeDistribution( 
//    		@QueryParam("startDate") final String startDate, 
//    		@QueryParam("endDate") final String endDate, 
//    		@QueryParam("networkElements") final String networkElements) {   
//    	try		{
//    		WebTarget tgt	=	serviceDiscovery.getMetricsDiameterErrorCodeDistributionService()
//    				.queryParam("startDate", startDate)
//    				.queryParam("endDate", endDate)
//    				.queryParam("networkElements", networkElements);
//    		if ( tgt == null)	{
//    			return Response.status(Response.Status.NOT_FOUND).build();    	
//    		}
//	    	Object usersArr	=	tgt.register(Object.class).request().get(Object.class);
//	    	return usersArr;
//    	}
//    	catch (Exception ex)	{
//    		ex.printStackTrace();
//    		return Response.status(Response.Status.NOT_FOUND).build();
//    	}
//
//    }   
    @GET
    @Path("diameter/errorCodeDistribution")
    @Produces({"application/json"})
    public Object getMetricsDiameterErrorCodeDistribution( 
    		@QueryParam("startDate") final Long startDate, 
    		@QueryParam("endDate") final Long endDate, 
    		@QueryParam("networkElements") final String networkElements,
    		@QueryParam("delta") final Integer delta) {   
    	try		{
    		WebTarget tgt	=	serviceDiscovery.getMetricsDiameterErrorCodeDistributionService()
    				.queryParam("startDate", startDate)
    				.queryParam("endDate", endDate)
    				.queryParam("networkElements", networkElements)
    				.queryParam("delta", delta);
    		if ( tgt == null)	{
    			return Response.status(Response.Status.NOT_FOUND).build();    	
    		}
	    	Object usersArr	=	tgt.register(Object.class).request().get(Object.class);
	    	return usersArr;
    	}
    	catch (Exception ex)	{
    		ex.printStackTrace();
    		return Response.status(Response.Status.NOT_FOUND).build();
    	}

    } 
}
