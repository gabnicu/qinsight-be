package com.radcom.qinsight.api.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import com.radcom.qinsight.services.impl.consul.ConsulDiscovery;
import com.radcom.qinsight.services.EnvVarServices;
import com.radcom.qinsight.services.definitions.discovery.ServiceDiscovery;

import com.radcom.qinsight.api.rest.tos.TransferObjDnsEntry;

@Stateless
@Path("networkElements")
public class ApiGatewayNetworkElements {
	
	@Inject
	@EnvVarServices
    ServiceDiscovery serviceDiscovery;
     
 
    @GET
    @Produces({"application/json"})
    public Object findAll( @QueryParam("path") String path, @QueryParam("include") String include) {   
    	try		{
	    	Object usersArr	=	serviceDiscovery.getNetworkElementsService().queryParam("path", path).queryParam("include", include).register(Object.class).request().get(Object.class);
	    	return usersArr;
    	}
    	catch (Exception ex)	{
    		ex.printStackTrace();
    		return Response.status(Response.Status.NOT_FOUND).build();
    	}

    }   

}
