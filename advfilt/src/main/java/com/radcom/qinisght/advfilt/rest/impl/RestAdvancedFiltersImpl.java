package com.radcom.qinisght.advfilt.rest.impl;

import java.util.List;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.core.Response;

import com.radcom.qinisght.advfilt.dao.DaoAdvancedFilters;
import com.radcom.qinisght.advfilt.rest.RestAdvancedFilters;
import com.radcom.qinisght.advfilt.rest.dto.AdvancedFilterTypes;
import com.radcom.qinisght.advfilt.rest.dto.DtoFilterEntry;
import com.radcom.qinsight.utils.rest.dtos.BasicDTO;

@Transactional
public class RestAdvancedFiltersImpl implements RestAdvancedFilters
{
	@Inject 
	DaoAdvancedFilters daoAdvFilt;

	@Override
	public Response getAllProcedureTypes() {

		
		List<DtoFilterEntry> lst	=	daoAdvFilt.getFilters( AdvancedFilterTypes.ADV_FILTER_DIAMETER_PROCEDURE_TYPE);
		BasicDTO dto	=	new BasicDTO();
		dto.setData( lst);	
		return Response.status(Response.Status.OK).entity(dto).build(); 
	}

}
