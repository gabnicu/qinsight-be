package com.radcom.qinisght.advfilt.rest.dto;

public enum AdvancedFilterTypes {
	ADV_FILTER_DIAMETER_PROCEDURE_TYPE("ADV_FILTER_DIAMETER_PROCEDURE_TYPE"),
	ADV_FILTER_DIAMETER_SUB_PROCEDURE_TYPE("ADV_FILTER_DIAMETER_SUB_PROCEDURE_TYPE"),
	ADV_FILTER_DIAMETER_APPLICATION_ID("ADV_FILTER_DIAMETER_APPLICATION_ID");	
	
	private final String text;	
	
	AdvancedFilterTypes ( final String txt) {
        this.text = txt;
    }

    @Override
    public String toString() {
        return text;
    }
}
