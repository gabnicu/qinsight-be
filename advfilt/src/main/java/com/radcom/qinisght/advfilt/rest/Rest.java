package com.radcom.qinisght.advfilt.rest;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import com.radcom.qinisght.advfilt.dao.DaoAdvancedFilters;
import com.radcom.qinisght.advfilt.rest.dto.AdvancedFilterTypes;
import com.radcom.qinisght.advfilt.rest.dto.DtoFilterEntry;
import com.radcom.qinsight.utils.rest.dtos.BasicDTO;

@Transactional
@Path("advfilt")
public class Rest {
	
	
	@Inject 
	DaoAdvancedFilters daoAdvFilt;

	
	
    @GET
    @Path("procType")
    @Produces({"application/json"})
    public Response findAll() {
		List<DtoFilterEntry> lst	=	daoAdvFilt.getFilters( AdvancedFilterTypes.ADV_FILTER_DIAMETER_PROCEDURE_TYPE);
		BasicDTO dto	=	new BasicDTO();
		dto.setData( lst);	
		return Response.status(Response.Status.OK).entity(dto).build(); 
    }

}
