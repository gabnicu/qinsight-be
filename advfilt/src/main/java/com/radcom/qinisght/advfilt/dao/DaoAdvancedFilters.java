package com.radcom.qinisght.advfilt.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.radcom.qinisght.advfilt.business.SqlProvider;
import com.radcom.qinisght.advfilt.rest.dto.AdvancedFilterTypes;
import com.radcom.qinisght.advfilt.rest.dto.DtoFilterEntry;




@RequestScoped
public class DaoAdvancedFilters {
	private static final Logger theLog = Logger.getLogger(DaoAdvancedFilters.class);
	
    @Resource(mappedName="java:jboss/VerticaDS") 
    private DataSource ds;
    
    
    @Inject
    SqlProvider sqlProvider;
    
    
    private static final String SQL_DIAMETER_PROCEDURE_TYPE	=	"SELECT * FROM omniq.CONVERSION_PROCEDURE_TYPE WHERE NAME LIKE 'DIAMETER%'";
    
//    public List<DtoFilterEntry> getFilters( AdvancedFilterTypes theType)
//    {
//    	Connection conn	= null;
//    	Statement stmt 	= null;
//    	ResultSet rs	= null;
//    	String theSQL	=	null;
//    	if ( theType.equals(AdvancedFilterTypes.ADV_FILTER_DIAMETER_PROCEDURE_TYPE))
//    		theSQL	=	SQL_DIAMETER_PROCEDURE_TYPE;
//    	theLog.info("Executing SQL: " + theSQL);
//    	
//    	List<DtoFilterEntry> theList	=	new ArrayList<DtoFilterEntry>();
//        try {
//			
//        	conn = ds.getConnection();
//        	
//			stmt = conn.createStatement();
//			String sql;
//
//			rs = stmt.executeQuery( theSQL);
//
//			while (rs.next()) {
//				DtoFilterEntry filterEntry	=	new DtoFilterEntry();
//
//				long id = rs.getLong("ID");
//				String name = rs.getString("NAME");
//				filterEntry.setId(id);
//				filterEntry.setName(name);
//				theList.add( filterEntry);				
//			}
//    	
//			theLog.error( "=========== Advanced Filters read entries #" + theList.size());
//        } 
//        catch (SQLException sqlEx)	{
//        	theLog.error( "SQL Exception", sqlEx);
//        	return null;
//        }
//        catch (Exception ex)	{
//        	theLog.error( "General Exception", ex);
//        	return null;
//        }
//        finally {
//        	try {
//        		if ( rs != null)
//        			rs.close();
//        		if ( stmt != null)
//        			stmt.close();
//        		if ( conn != null)
//        			conn.close();
//        	}
//        	catch (Exception ex2) {
//        		theLog.error( "Exception closing connection", ex2);
//        	}
//        }
//        
//        theLog.info( "Read Advance Filters From Verical. Number of lines read is: " + theList.size());
//        return theList;
//    }

    
    public List<DtoFilterEntry> getFilters( AdvancedFilterTypes theType)
    {

    	String theSQL	=	null;
    	String theParamName	=	null;
    	if ( theType.equals(AdvancedFilterTypes.ADV_FILTER_DIAMETER_PROCEDURE_TYPE))	{
    		theSQL	=	sqlProvider.getProperty("DIAMETER_PROCEDURE_TYPE_SQL");
    		theParamName	=	sqlProvider.getProperty("DIAMETER_PROCEDURE_TYPE_PARAM");
    	}
    	theLog.info("Executing SQL: " + theSQL + ", theParamName=" + theParamName);
    	return getGenericFilters( theSQL, theParamName);
    }    
    
    
    
    
    public List<DtoFilterEntry> getGenericFilters( String theSQL, String theParamName)
    {
    	Connection conn	= null;
    	Statement stmt 	= null;
    	ResultSet rs	= null;

    	theLog.info("Executing SQL: " + theSQL);
    	
    	List<DtoFilterEntry> theList	=	new ArrayList<DtoFilterEntry>();
        try {
			
        	conn = ds.getConnection();
        	
			stmt = conn.createStatement();
			String sql;

			rs = stmt.executeQuery( theSQL);

			while (rs.next()) {
				DtoFilterEntry filterEntry	=	new DtoFilterEntry();

				long id = rs.getLong("ID");
				String name = rs.getString( theParamName);
				filterEntry.setId(id);
				filterEntry.setName(name);
				theList.add( filterEntry);				
			}
    	
			theLog.error( "=========== Advanced Filters read entries #" + theList.size());
        } 
        catch (SQLException sqlEx)	{
        	theLog.error( "SQL Exception", sqlEx);
        	return null;
        }
        catch (Exception ex)	{
        	theLog.error( "General Exception", ex);
        	return null;
        }
        finally {
        	try {
        		if ( rs != null)
        			rs.close();
        		if ( stmt != null)
        			stmt.close();
        		if ( conn != null)
        			conn.close();
        	}
        	catch (Exception ex2) {
        		theLog.error( "Exception closing connection", ex2);
        	}
        }
        
        theLog.info( "Read Advance Filters From Verical. Number of lines read is: " + theList.size());
        return theList;
    }

}
