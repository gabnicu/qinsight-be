package com.radcom.qinsight.utils.time;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import com.radcom.qinsight.utils.constants.AppConstants;

public class TimeUtils {
	
	public static long getCurrentUtcAsLong()
	{
		long t3 = Calendar.getInstance(TimeZone.getTimeZone("UTC")).getTimeInMillis();
		System.out.println( "UTC: " + t3);
		return t3;
	}
	
	public static String obtainStringFromUtcLong( long millisecs)
	{
		String sz	=	Instant.ofEpochMilli( millisecs).toString();
		System.out.println( "UTC: " + sz);
		return sz;
	}
	
	public static String obtainSqlStringFromUtcLong( long millisecs)
	{
		String sz	=	obtainStringFromUtcLong( millisecs);
		sz	=	sz.replaceAll( "T", " ");
		sz	=	sz.replaceAll( "Z", "");
		sz	=	sz + ".0000000";
		System.out.println( "SQL UTC: " + sz);
		return sz;
	}
	
	public static String convertDateFromYYYYMMDDtoMMMDD( String initialDate)
	{
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
	    try {

	        Date date = formatter.parse(initialDate);
	        DateFormat out = new SimpleDateFormat("MMM dd");
	        return (out.format(date));

	    } catch (ParseException e) {
	        e.printStackTrace();
	        return null;
	    }
	}
	public static String convertDateFromYYYYMMDDtoMMMYYYY( String initialDate)
	{
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
	    try {

	        Date date = formatter.parse(initialDate);
	        DateFormat out = new SimpleDateFormat("MMM YYYY");
	        return (out.format(date));

	    } catch (ParseException e) {
	        e.printStackTrace();
	        return null;
	    }
	}
	
	public static int getYearFromDateYYYYMMDD( String date)
	{
		if ( date == null)
			return AppConstants.NON_VALID_POSITIVE_VALUE;
		if ( date.length() < 4)
			return AppConstants.NON_VALID_POSITIVE_VALUE;
		String year	=	date.substring(0,  4);
		return Integer.parseInt(year);
		
	}
	public static int getMonthFromDateYYYYMMDD( String date)
	{
		if ( date == null)
			return AppConstants.NON_VALID_POSITIVE_VALUE;
		if ( date.length() < 7)
			return AppConstants.NON_VALID_POSITIVE_VALUE;
		String month	=	date.substring(5,  7);
		if ( month.startsWith("0"))
			month	=	month.substring(1);
		return Integer.parseInt(month);
		
	}
	public static int getDayFromDateYYYYMMDD( String date)
	{
		if ( date == null)
			return AppConstants.NON_VALID_POSITIVE_VALUE;
		if ( date.length() < 10)
			return AppConstants.NON_VALID_POSITIVE_VALUE;
		String month	=	date.substring(8,  10);
		if ( month.startsWith("0"))
			month	=	month.substring(1);
		return Integer.parseInt(month);
		
	}
	
	public static String generateIsoDateStartOfDay( String date)
	{
		String part1	=	date.substring(0, 10);		
		return part1 + " 00:00:00";		
	}
	public static String generateIsoDateEndOfDay( String date)
	{
		String part1	=	date.substring(0, 10);		
		return part1 + " 23:59:59";		
	}
	
	public static String generateIsoDateEndOfYear( String szYear)
	{
		
		return szYear + "-12-31 23:59:59";		
	}
	public static String generateIsoDateStartOfYear( String szYear)
	{
		
		return szYear + "-01-01 00:00:00";		
	}
	
	public static String generateIsoDateEndOfMonth( String date)
	{
		String dt	=	computeLastDayOfMonth( date);
		return dt + " 23:59:59";		
	}
	public static String generateIsoDateStartOfMonth( String date)
	{
		
		return date.substring(0, 7) + "-01 00:00:00";		
	}
	
	
	public static String generateIsoDateEndOfQuarter( String date)
	{
		return "2018-06-30 23:59:59";		
	}
	public static String generateIsoDateStartOfQuarter( String date)
	{
		
		return "2018-04-01 00:00:00";		
	}
	
	public static String generateAngloSaxonDate( String date)
	{
		if ( date == null)
			return null;
		if ( date.length() < 10)
			return null;
		String year		=	date.substring(0,  4);
		String month	=	date.substring(5,  7);
		String day		=	date.substring(8,  10);
		
		return month + "/" + day + "/" + year;
		
	}

	
	public static String generateAngloSaxonDateStartOfQuarterFromDateYYYYMMDD( String date)
	{
		if ( date == null)
			return null;
		if ( date.length() < 10)
			return null;
		int year		=	getYearFromDateYYYYMMDD(date);
		int month		=	getMonthFromDateYYYYMMDD(date);
		int day		=	getDayFromDateYYYYMMDD(date);
		
		String monthExt	=	null;
		if ( month <=3)
			monthExt	=	"01";
		else if ( month <=6)
			monthExt	=	"04";
		if ( month <=9)
			monthExt	=	"07";
		if ( month <=3)
			monthExt	=	"10";		
		return monthExt + "/01/" + year;	
	}

	
	// parameter date is of format yyyy-MM-dd
	public static String computeLastDayOfMonth(String date)
	{
		LocalDate localDate = LocalDate.parse(date,DateTimeFormatter.ofPattern("yyyy-MM-dd"));
		LocalDate lastDayOfMonth = localDate.with(TemporalAdjusters.lastDayOfMonth());
		String szRet	=	lastDayOfMonth.toString();
		System.out.println("Last Day of Month: " + szRet);  
		return szRet;
	}

	public static String getIsoStringFromLong( long theTime, String timeZone) {
		if ( timeZone == null)
			timeZone	=	"GMT+3:00";
	    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	    sdf.setTimeZone(TimeZone.getTimeZone(timeZone));
	    String date = sdf.format( theTime);
	    System.out.println( "ISO TIme: " + date);
	    return date;
	}
	public static long getLongFromIsoString( String isoDate, String timeZone)
	{
		if ( timeZone == null)
			timeZone	=	"GMT+3:00";
	
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date d;
		try {
			d = sdf.parse( isoDate);
			Calendar cal	=	Calendar.getInstance(TimeZone.getTimeZone(timeZone));
			cal.setTime( d);
			System.out.println( "UTC: " + d.getTime() + " --- " + cal.getTimeInMillis());
			return cal.getTimeInMillis();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return -1L;
		}		
	}
	
	public static String getGmtStringFromDeltaInMinutes( int delta)
	{
		switch ( delta)	{
			case 0:
				return "GMT+0.00";
			case -60:
				return "GMT+1.00";
			case -120:
				return "GMT+2.00";
			case -180:
				return "GMT+3.00";
			case 60:
				return "GMT-01.00";
			case 120:
				return "GMT-02.00";
			case 180:
				return "GMT-03.00";
			case 240:
				return "GMT-04.00";
			case 300:
				return "GMT-05.00";
			case 360:
				return "GMT-06.00";
			case 420:
				return "GMT-07.00";
			case 480:
				return "GMT-08.00";
			case 540:
				return "GMT-09.00";
			case 600:
				return "GMT-10.00";

			default:
				return "GMT+3.00";
				
		}
	}
	
	public static String getQuarterFromDate( String date)
	{
		int month	=	getMonthFromDateYYYYMMDD( date);
		int year	=	getYearFromDateYYYYMMDD(date);
		String ret	=	null;
		if ( month <=3)	{
			ret	=	"Quarter 1, ";
		}
		else if ( month <=6)	{
			ret	=	"Quarter 2, ";
		}
		else if ( month <=9)	{
			ret	=	"Quarter 3, ";
		}
		else if ( month <=12)	{
			ret	=	"Quarter 4, ";
		}
		ret += year;
		return ret;
	}

}
